<?php
/**
 * OrderNumber gm40_w1_pc0_1475143772
 * gm40 GroupMember->id
 * w1 week 1
 * pc0 personal_curator
 * 1475143772 Unique
 */

namespace app\controllers;


use app\models\GiveHasUser;
use app\models\Gives;
use app\models\PaymentLer;
use app\models\Promotion;
use app\models\PromotionHasUser;
use app\models\User;
use app\services\payment\PaymentFactory;
use Faker\Provider\DateTime;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Request;

class PaymentController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
//                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionCheck()
    {
        if (!Yii::$app->request->isPost) {
            throw new NotFoundHttpException();
        }
        $paymentGateway = PaymentFactory::createPaymentGateway($this->getPaymentType());
        $paymentData = $paymentGateway->getPaymentData(Yii::$app->request);
        if ($this->isValidOrder($paymentData->amount, $paymentData->give_id) && $paymentGateway->isValidPayment(Yii::$app->request)) {
            $this->setDataBaseVal($paymentData);
            return $paymentGateway->renderSuccessCheckView();
        } else {
            return $paymentGateway->renderFailedCheckView();
        }
    }

    public function setDataBaseVal($paymentData)
    {
        Yii::$app->db->createCommand()->update(User::tableName(), [
            'invoiceId' => $paymentData->invoiceId,
        ], [
            'id' => $paymentData->userId,
        ])->execute();
    }

    public function actionAviso()
    {
        if (!Yii::$app->request->isPost) {
            throw new NotFoundHttpException();
        }

        $paymentGateway = PaymentFactory::createPaymentGateway($this->getPaymentType());
        $paymentData = $paymentGateway->getPaymentData(Yii::$app->request);
        if ($this->isValidOrder($paymentData->amount, $paymentData->give_id) && $paymentGateway->isValidPayment(Yii::$app->request)) {
            $cost = (int)$paymentData->amount;
            $user = User::findOne(['invoiceId' => $paymentData->invoiceId]);
            $give = Gives::findOne(['all_cost' => $cost, 'id' => $paymentData->give_id]);
            $promotion = Promotion::findOne(['all_cost' => $cost, 'id' => $paymentData->give_id]);
            if ($user != null && $give != null) {
                $payment = new PaymentLer();
                $payment->cost = $cost;
                $payment->date_create = date('Y-m-d H:i:s');
                $payment->user_id = $user->id;
                if ($payment->save(false)) {
                    $GHU = new GiveHasUser();
                    $GHU->user_id = $user->id;
                    $GHU->give_id = $give->id;
                    if ($GHU->save(false)) {
                        Yii::$app->mailer
                            ->compose('success_pay', ['user' => $user, 'give' => $give])
                            ->setSubject('Спасибо за покупку спонсорского места!')
                            ->setTo($user->email)
                            ->send();
                        Yii::$app->mailer
                            ->compose('success_pay_admin', ['user' => $user, 'give' => $give])
                            ->setSubject('Успешная оплата гива')
                            ->setTo('info@givecorp.ru')
                            ->send();
                    }
                }
            } elseif ($user != null && $promotion != null) {
                $payment = new PaymentLer();
                $payment->cost = $cost;
                $payment->date_create = date('Y-m-d H:i:s');
                $payment->user_id = $user->id;
                if ($payment->save(false)) {
                    $GHU = new PromotionHasUser();
                    $GHU->user_id = $user->id;
                    $GHU->promotion_id = $promotion->id;
                    if ($GHU->save(false)) {
                        Yii::$app->mailer
                            ->compose('success_pay', ['user' => $user, 'give' => $give])
                            ->setSubject('Спасибо за покупку спонсорского места!')
                            ->setTo($user->email)
                            ->send();
                        Yii::$app->mailer
                            ->compose('success_pay_admin_promotion', ['user' => $user, 'promotion' => $promotion])
                            ->setSubject('Успешная оплата Рекламы')
                            ->setTo('info@givecorp.ru')
                            ->send();
                    }
                }

            }

            return $paymentGateway->renderSuccessView();

        } else {
            return $paymentGateway->renderFailedView();
        }
    }

    private
    function getPaymentType()
    {
        return PaymentFactory::getPaymentTypeFromRequest(Yii::$app->request);
    }


    /**
     * @param $sum
     * @return bool
     */
    private
    function isValidOrder($sum, $give_id)
    {

        $give = Gives::findOne($give_id);

        if ((int)$give->all_cost == (int)$sum) {
            return true;
        } else {
            $promotion = Promotion::findOne($give_id);
            if ((int)$promotion->all_cost == (int)$sum) {
                return true;
            } else {
                return false;
            }
        }
    }


    public
    function sendMail($user, $group, $summa)
    {
    }


    public
    function actionSuccess()
    {
        return $this->render('success');
    }

    public
    function actionFail()
    {
        $this->layout = 'payment';
        return $this->render('fail');
    }
}