<?php
/**
 * OrderNumber gm40_w1_pc0_1475143772
 * gm40 GroupMember->id
 * w1 week 1
 * pc0 personal_curator
 * 1475143772 Unique
 */

namespace app\controllers;


use app\models\GiveHasUser;
use app\models\Gives;
use app\models\PaymentLer;
use app\models\Promotion;
use app\models\PromotionHasUser;
use app\models\User;
use app\services\payment\PaymentFactory;
use Faker\Provider\DateTime;
use ruskid\stripe\Stripe;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\CardException;
use Stripe\PaymentIntent;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Request;

class StripeController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
//                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->layout = false;
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionCreatePaymentIntent()
    {
        $this->enableCsrfValidation = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
// Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey('sk_live_uElYk80sy9xSZSadZXCsKI3z00TNZqDgNL');
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '.log';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        try {
            $intent = \Stripe\PaymentIntent::create([
                'amount' => 99000,
                'currency' => 'usd',
                // Verify your integration in this guide by including this parameter
                'metadata' => ['integration_check' => 'accept_a_payment'],
            ]);
        } catch (ApiErrorException $e) {
            file_put_contents($file, PHP_EOL . json_encode($e->getMessage()), FILE_APPEND);
        }
        $user = User::findOne($_POST['user_id']);
        if ($user) {
            $payment = PaymentLer::findOne(['user_id' => $_POST['user_id']]);
            if (!$payment) {
                $payment = new PaymentLer();
            }
            $payment->date_create = date('Y-m-d H:i:s');
            $payment->cost = 990;
            $payment->user_id = $user->id;
            $payment->stripe_id = $intent->id;
            if ($payment->save(false)) {
                $output = [
                    'publishableKey' => 'pk_live_lCPEn9JA3fQ9VUhn53pwmUCP00UjEwbQyX',
                    'clientSecret' => $intent->client_secret,
                ];
                return $output;
            }
            return ['error' => true];
        }
        return ['error' => true];
    }

    public function actionPaymentChecker()
    {
        // Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey('sk_live_uElYk80sy9xSZSadZXCsKI3z00TNZqDgNL');

// If you are testing your webhook locally with the Stripe CLI you
// can find the endpoint's secret by running `stripe listen`
// Otherwise, find your endpoint's secret in your webhook settings in the Developer Dashboard
        $endpoint_secret = 'whsec_N5Mywr3NG6rmZg7PTKa2QWG5hy5mFOGO';

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '.log';
        if (!is_file($file)) {
            fopen($file, "w", true);
        }

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            file_put_contents($file, PHP_EOL . json_encode($e->getMessage()), FILE_APPEND);
            file_put_contents($file, PHP_EOL . json_encode($payload), FILE_APPEND);
            http_response_code(400);
            exit();
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
            file_put_contents($file, PHP_EOL . json_encode($e->getMessage()), FILE_APPEND);
            file_put_contents($file, PHP_EOL . json_encode($payload), FILE_APPEND);
            http_response_code(400);
            exit();
        }
        file_put_contents($file, PHP_EOL . json_encode($payload), FILE_APPEND);

// Handle the event
        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object; // contains a StripePaymentIntent
                $this->successPayment($paymentIntent);
                break;
            default:
                // Unexpected event type
                http_response_code(400);
                exit();
        }

        http_response_code(200);
    }

    public function actionPayment()
    {
        return $this->render('payment');
    }

    public function successPayment(PaymentIntent $paymentIntent)
    {
        $file = Yii::getAlias('@runtime') . '/logs/' . date('Y-m-d') . '.log';
        $payment = PaymentLer::findOne(['stripe_id' => $paymentIntent->id]);
        if ($payment) {
            $payment->status = true;
            return $payment->save(false);
        }else{
            file_put_contents($file, PHP_EOL . json_encode([$paymentIntent->id => 'Не найдено в оплатах']), FILE_APPEND);
        }
    }
}