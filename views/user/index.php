<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', isset($_GET['status']) && $_GET['status'] == 1 ? 'Заявки на консультацию' : 'Заявки на оплату');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::beginForm(['user/check-status'], 'post'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать участника'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Заявки на консультацию'), \yii\helpers\Url::to(['/user/index', 'status' => 1]), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Заявки на оплату'), \yii\helpers\Url::to(['/user/index', 'status' => 0]), ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            'first_name',
            'instagram_ak',
            'email',
            'phone',
            [
                'attribute' => 'payment_status',
                'filter' => Html::dropDownList('payment_status', $_GET['payment_status'], ['' => 'Выбрать'] + \app\models\PaymentLer::getStatusesArray()),
                'format' => 'raw',
                'value' => function (\app\models\User $model) {
                    return Html::tag('span', ucfirst(($model->payment ? $model->payment->getStatusString() : 'Не оплачен')), ['class' => $model->payment ? $model->payment->getLabelCssClassByStatus() : 'label label-danger']);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {view} {success} {cancel}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', \yii\helpers\Url::to(['/user/update', 'id' => $model->id]), [
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', \yii\helpers\Url::to(['/user/delete', 'id' => $model->id]), [
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', \yii\helpers\Url::to(['/user/view', 'id' => $model->id]), [
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>
    <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-primary', 'name' => 'success']); ?>
    <?= Html::submitButton('Оменить', ['class' => 'btn btn-danger', 'name' => 'cancel']); ?>
    <?= Html::endForm(); ?>
</div>
