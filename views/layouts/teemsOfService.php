<html lang="en">
<head>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '605878313672331');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=605878313672331&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Terms of service</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/live.min.css" rel="stylesheet">

    <meta name="robots" content="noindex">

</head>
<body>
<div class="wrapper">
    <div class="page">
        <div class="container">
            <header class="header" style="display: inline-flex; width: 80%; align-items: center;margin-bottom: 50px">
                <div class="header__left"
                     style="background: url(/img/logo2.png) center no-repeat; width: 13rem; height: 10rem">
                </div>
                <div style="margin-left: auto; font-size: 10px;color: grey">INFO@GIVEAWAYCORP.COM |
                    WWW.GIVEAWAYGODS.US
                </div>
            </header>

            <div class="row">
                <div class="col-md-12">
                    <h1>TERMS AND DEFINITIONS.</h1>
                    <p><strong>Offer</strong> <br> This document is published on the website <a
                                href="https://giveawaygods.us/">giveawaygods.us</a></p>
                    <p><strong>Confirmation of the offer</strong><br>
                        (we will have a checkbox with a check mark before payment and a link to the text itself. we Need
                        to indicate that the check mark indicates acceptance of the terms of the offer.)
                    </p>
                    <p><strong>Participants</strong><br>an instagram person who has satisfied all of the eligibility
                        requirements for the giveaway by following the rules posted by GAC and the Influencer
                    </p>
                    <p><strong>Sponsor</strong>&nbsp;<br>
                        a person or entity that has paid GAC for to market their Instagram profile through an active
                        giveaway promotion.
                    </p>
                    <p><strong>Influencer<br></strong>
                        a person or entity hired by GAC to promote the active giveaway promotion
                    </p>
                    <p>
                        <strong>Contract </strong>- an agreement between the Sponsor and the Influencer for the
                        provision of information services, which is concluded by Accepting the Offer
                    </p>
                    <h2>TERMS & CONDITIONS</h2>
                    <p>Instructions on how to enter
                        that are set out on Instagram (or online) form part of these terms
                        and conditions (T&Cs).
                    </p>
                    <p style="margin-right: 7pt;">1.
                        The attached Schedule sets out the
                        defined terms within these T&Cs and forms part of these T&Cs. If there
                        is any inconsistency or discrepancy between the Schedule and these T&Cs the
                        Schedule prevails. To the extent there is a conflict between T&Cs appearing
                        on Instagram, or any other location and any terms of use or other agreements
                        posted on GAC’s website, the terms found on GAC’s website are the T&Cs to
                        be abided by.
                    </p>
                    <p>2.
                        The Promotion is subject
                        to all applicable federal, state,
                        and local laws and regulations.&nbsp;Participation constitutes the participant's full and
                        unconditional agreement to these
                        T&Cs and any GAC
                        decisions, which are final and binding in all matters
                        related to the Promotion. Winning
                        a prize is contingent
                        upon fulfilling all requirements set forth herein.
                    </p>
                    <p style="margin-right: 5pt;">3.
                        Entry is open
                        only to legal residents of the Eligible States/Territories who satisfy the
                        Method of entry. Directors, employees and contractors, (and all immediate
                        family members of Directors, employees and contractors) of GAC and their associated
                        agencies, companies, subcontractors, and any related
                        entities (and all immediate family members of associated agencies, companies,
                        subcontractors, and any related
                        entities) are not eligible to enter.
                    </p>
                    <p style="margin-right: 13pt;">4.
                        The Promotion will take place during the Promotion period set out in the attached “Giveaway
                        Schedule of Details.“
                    </p>
                    <p>5.
                        The Prize(s) are set out in the attached
                        “Giveaway Schedule at Details”.
                    </p>
                    <p style="margin-right: 7pt;">6.
                        The total value
                        of the prize pool set out in the attached Schedule at Total prize pool. Prize
                        values are in USD unless stated
                        to the contrary. Prize values
                        listed in the Schedule are the recommended retail values as provided by the supplier
                        and are correct
                        at the time of printing.
                        GAC does not accept responsibility for any variation in
                        the value of The Prize(s).
                    </p>
                    <p style="margin-right: 6pt;">7.
                        If any prize is unavailable for whatever reason,
                        GAC reserves the right to substitute the prize for an alternate prize of equal or greater
                        value,
                        subject to State Regulation.
                    </p>
                    <p style="margin-right: 9pt;">8.
                        Unless approved by GAC, the
                        prize(s) are not transferable or exchangeable, nor is the prize redeemable for
                        cash. GAC is not responsible for any lost or stolen prizes and GAC takes no
                        responsibility for prizes damaged, delayed or lost in transit.
                    </p>
                    <p>9.
                        If any part of the prize contains any cash
                        component. The cash component will be transferred via&nbsp;PayPal or Bank
                        transfer (at GACs discretion) to the winners nominated account. The&nbsp;GAC
                        is not responsible for any lost
                        or stolen cash
                        transferred to the
                        Prize winner, nor is GAC responsible for any
                        error in banking
                        or PayPal details
                        provided by the Prize Winner.
                        If for any reason Banking
                        or PayPal is unavailable as a method of
                        transferring any cash component an alternate method may be selected at GAC’s
                        absolute discretion.
                    </p>
                </div>
                <div>
                    <p style="margin-right: 6pt;">10. The Prize winner
                        should seek independent financial advice prior
                        to acceptance of their prize(s)
                        to determine if any tax implications that may arise from their prize winning.
                        (including but not limited to customs duties and taxes and income
                        tax). GAC accept
                        no responsibility for any liability or tax implications that may arise
                        in connection with the winner accepting this Prize.
                    </p>
                    <p style="margin-right: 27pt;">11. It is a condition of accepting the Prize that the winner
                        may be required to sign a legal release in a form determined by GAC in its absolute
                        discretion.
                    </p>
                    <p style="margin-right: 21pt;">12. It is a condition of accepting the Prize that the winner
                        may be required to provide
                        valid photographic ID together with proof of address.
                    </p>
                    <p style="margin-right: 10pt;">13. It is a condition of accepting the Prize that the winner
                        is required to post an “I won @influencer’s instagram giveaway!” image on their public
                        Instagram account for a period of up to seven (7) days after the announcement
                        of the Prize winner.
                    </p>
                    <p style="margin-right: 7pt;">14. It
                        is a condition of accepting the Prize that the winner is required to post a
                        photograph of their prize on their
                        public Instagram account, tagging @giveawaygods.us within seven (7) days of
                        receiving the prize items, and in this regard:
                    </p>
                    <p style="margin-right: 25pt; margin-left: 77pt;">a.
                        The winners Instagram account
                        must remain public
                        for at least seven (7) days after posting the photograph of themselves with their prize;
                    </p>
                    <p style="margin-left: 102px;">b.
                        If part of the prize consists of cash:
                    </p>
                    <p style="margin-right: 16pt; margin-left: 113pt;">1.
                        the winner
                        may be required to post a photograph of themselves with cash, or a novelty cheque or other
                        promotional material as provided
                        by Giveaway Corp Inc..
                    </p>
                    <p style="margin-right: 9pt; margin-left: 113pt;">2. Giveaway Corp Inc. reserves the right to
                        withhold no more than 50% of the payment
                        due to the winner pending receipt
                        and posting of the photograph referred to in this clause.
                    </p>
                    <p style="margin-right: 7pt;">15. It
                        is a condition of accepting the Prize that the winner is required to provide
                        GAC with a 10-15 second video of
                        themselves with the prize within seven (7) days of receiving the prize items,
                        and in this regard. The winner agrees that this video will be used for marketing and promotional
                        purposes on GACs website and social media accounts.
                    </p>
                </div>
                <div>
                    <p>winner
                        agrees that this video will be used
                        for marketing and promotional purposes
                        on GACs website
                        and social media accounts.
                        <br>
                    </p>
                    <p style="margin-right: 30pt;">16. Each valid entrant
                        who has entered
                        the Promotion over the duration
                        of the Promotional period will be
                        entered into the draw.
                    </p>
                    <p style="margin-right: 12pt;">17. The entrants must follow the Method of entry during
                        the Promotion period
                        to enter the Promotion. Failure to do so will result in an
                        invalid entry and GAC has no obligation to inform the entrant if their entry is
                        invalid.
                    </p>
                    <p>18. Entrants must have a valid,
                        public Instagram account. Only publicly viewable Instagram accounts that have
                        followed the Host Instagram account
                        and all accounts
                        listed in the Host Instagram&nbsp;Account following list will be considered valid
                        entries. To enter the Sweepstake entrants must:
                    </p>
                    <p style="margin-right: 28pt; margin-left: 77pt;">a.
                        Follow the Host Instagram account(s) and all accounts listed
                        in the host Instagram(s) accounts “Following” list of accounts
                        during the Sweepstakes Period;
                    </p>
                    <p style="margin-left: 77pt;">b.
                        Entrants can obtain bonus entries by:
                    </p>
                    <p style="margin-right: 0cm; margin-left: 113pt;"><em>1.
                        </em><em>N/A</em>
                    </p>
                    <p><em> </em>
                    </p>
                    <p style="margin-right: 20pt; margin-left: 77pt;">c.
                        There is a permitted limit of one (1) entries
                        per person. Multiple
                        entries will be deleted without notice.
                    </p>
                    <p style="margin-right: 35pt; margin-left: 77pt;">d. Odds of winning
                        a prize will depend on the total
                        number of eligible
                        entries received for the
                        random draw.
                    </p>
                    <p style="margin-right: 8pt;">19. In the unlikely
                        event that any Instagram account
                        associated with this
                        campaign is offline
                        at any stage during the
                        campaign, GAC may, at their absolute discretion, substitute another account in
                        lieu of the offline account for any period of time GAC deems necessary. In this
                        event, an entrant who follows all required accounts including the substitute account (but
                        not the offline
                        account) will also be deemed to be following
                        all required accounts and their entry will be a valid entry into the draw.
                    </p>
                    <p style="margin-right: 9pt;">20. All entries become
                        the sole property
                        of GAC and receipt of entries will not be acknowledged. GAC is not responsible for
                        telecommunications, network,
                        electronic, technical or computer failure
                        or any kind of lost, late, incomplete, mutilated, invalid,
                        unintelligible, misdirected entries,
                        all of which will be disqualified. Only completed entries with valid email
                        addresses or social media accounts, as applicable, are eligible. All ineligible
                        entries will be disqualified.
                    </p>
                </div>
                <p>21. In case of dispute as to the identity of participant, entry
                    will be declared
                    made by the authorized account holder of the email address or social media account used to
                    submit
                    the entry.&nbsp;“Authorized Account Holder” is defined
                    as the natural person who is assigned the account by the email provider, online
                    service provider, social media provider
                    or other organization. Any other attempted
                    form of entry is prohibited; no automatic, programmed; robotic or similar
                    means of entry
                    are permitted.
                </p>
                <div>
                    <p style="margin-right: 16pt;">22. GAC accepts no responsibility for any late
                        or ineligible entries, or any entries
                        not received for whatsoever
                        reason.
                    </p>
                    <p style="margin-right: 7pt;">23. The
                        prize(s) will be awarded to the valid entrant(s) drawn randomly in accordance
                        with the Prize draw details. GAC may draw additional reserve entries (and
                        record them in order). In the event of an invalid entry or an ineligible
                        entrant, or if the entrant is ineligible to accept the prize, the prize will be
                        awarded to the first reserve entry
                        drawn. If the prize can’t be awarded to the entrant drawn, GAC will then
                        continue this process until the prize is awarded.
                    </p>
                    <p> 24.
                        The winner does not need to be present
                        at the draw.
                    </p>
                    <p style="margin-right: 8pt;">25. The winner(s) will be notified by the method nominated in
                        the Notification of Winners section of the Schedule. The Prize winner
                        will have been deemed to have been notified on the earlier
                        of when the winner
                        actually receives and views the notification from GAC or two calendar
                        days thereafter.
                    </p>
                    <p style="margin-right: 6pt;">26. Each entrant is responsible for monitoring Facebook and
                        Instagram for the notification and communications
                        regarding this draw. GAC has no liability for potential winner’s failure to
                        receive notices due to filters, security settings or provision of incorrect or
                        non-functioning contact
                        details. If the drawn winner
                        is unable to be
                        contacted or if the prize
                        is undeliverable using the contact
                        details provided, the Prize winner shall forfeit the prize.
                    </p>
                    <p>27. The winner(s) name,
                        state/territory of residence, Instagram handle and photograph, will
                        be published online in accordance with the Public announcement of winners section
                        of the Schedule.
                    </p>
                    <p>28. If the prize(s)
                        is not been claimed by the Unclaimed
                        prize draw time and date and subject
                        to any written directions from a State lottery
                        agency, GAC may conduct an Unclaimed prize
                        draw. If this
                        occurs, GAC will contact the Unclaimed Prize
                        draw winner by the same
                        methods set out herein for the original
                        Prize winner.
                    </p>
                    <p>29. The
                        Prize winner acknowledges and agrees that except where permitted by law, the
                        total cumulative liability of GAC in connection with this Agreement and the
                        promotion is limited, in GAC’s discretion, to either resupplying Prize items,
                        or paying the cost of resupplying Prize items. GAC will have no liability for
                        any indirect loss or damage suffered by any entrant or any Prize winner(s).
                    </p>
                </div>
                <div>
                    <p style="margin-right: 6pt;">30. Except where unable
                        to be excluded by law,
                        entrants and Prize
                        winner(s) release and
                        indemnify GAC and
                        its related entities from and against
                        all actions, penalties, liabilities, claims or demands against
                        GAC or that GAC may incur
                        for any loss
                        or damage which
                        is or may be suffered or sustained as a direct
                        or indirect result of an entrant entering or winning
                        the Promotion.
                    </p>
                    <p style="margin-right: 13pt;">31. Subject to any requirement by a regulatory authority, GAC
                        may, in it’s absolute discretion, not accept or disqualify a particular entry,
                        or cancel the entire Promotion at any time
                        without giving reasons
                        and without liability to any entrants,
                    </p>
                    <p>32. GAC
                        reserves the right to verify the validity of entries and Prize winners, and to
                        dismiss any entry that is misleading or fraudulent, or if it is suspected that
                        an entrant has manipulated the entry process via using multiple or fake
                        accounts, or any other method that GAC deems to be unscrupulous.
                    </p>
                    <p>33. Subject to the approval of any relevant authorities, where the Promotion is incapable of
                        running as planned
                        for any reason, including (but not limited to) early deletion of the campaign
                        post due to any third party Artificial Intelligence software, early or accidental deletion
                        of any campaign posts
                        by any other
                        person, any outage of any
                        social media accounts, war, terrorism, state of emergency or disaster
                        (including natural disaster), infection by computer virus,
                        bugs, tampering, unauthorised intervention, technical failures which corrupt or affect the
                        administration, security,
                        fairness, integrity or proper conduct of the campaign, any alteration to any social
                        media terms of service, access
                        or permission in such a way that
                        adversely affects the Promotion, the Promotion may be suspected
                        for the duration
                        of the event and/or GAC may, in it’s absolute discretion, cancel or recommence the Promotion
                        in it’s entirety.
                    </p>
                    <p>34. Entrants consent to GAC using the personal
                        information provided in connection with this promotion for awarding of any prizes, including
                        to third parties
                        involved in the promotion and any relevant
                        authorities. In addition to any use that may be outlined
                        in GAC’s Privacy
                        Policy.
                    </p>
                    <p>35. Each winner’s acceptance of a prize
                        constitutes permission (except
                        where prohibited) for
                        GAC to use
                        the winner’s name, Instagram handle, photograph, video footage,
                        likeness, statements, biographical information, voice and address (city and
                        state) for any and all public relations, advertising and/or promotional purposes
                        as determined by GAC, in all forms
                        of media and by all
                        manners (now and hereafter
                        known), in perpetuity, without notice,
                        consent, review or approval or further compensation.
                    </p>
                    <p>36. The collection and disclosure of personal information provided in connection with this
                        promotion will be handled in accordance with
                        GAC's Privacy statement which adheres to the USA’s
                        Privacy Act 1974
                        and other applicable American Privacy
                        Laws.
                    </p>
                    <p>37. The names of individuals, INCLUDING the Talent sponsoring this Promotion, the
                        campaign host, groups, companies, products and services
                        mentioned herein, and any corresponding likenesses, logos and images&nbsp;thereof reproduced
                        herein, have been used for identification
                        purposes only and may be the copyrighted properties and trademarks of their
                        respective owners. The mention of any individual, INCLUDING the Talent hosting
                        this Promotion, group or company, including INSTAGRAM or FACEBOOK, or the
                        inclusion of a product or service as a prize, does not imply any association
                        with or endorsement by such individual, group or company or the manufacturer or
                        distributor of such product or service and, except as otherwise indicated, NO
                        ASSOCIATION OR ENDORSEMENT IS INTENDED OR SHOULD BE INFERRED. The&nbsp;invalidity or
                        unenforceability of any provision of these T&Cs
                        or the Prize Acceptance Release
                        will not affect the validity or enforceability of any other provision. In the event that any
                        provision of the T&Cs
                        or the Prize Acceptance Release is determined to be invalid
                        or otherwise unenforceable or illegal, the
                        other provisions will remain in effect
                        and will be construed in accordance with their terms as if the invalid
                        or illegal provision were not contained herein.
                        The Promotor’s failure
                        to enforce any term of these T&Cs
                        will not constitute a waiver of that provision.
                    </p>
                </div>
                <div>
                    <p style="margin-right: 9pt;">38. The Promotion and these Terms of entry will be governed by the
                        law of the State or Territory in which GAC ordinarily resides. Entrants accept
                        the non-exclusive jurisdiction of courts and tribunals of that State or
                        Territory in connection with disputes
                        concerning the Promotion.
                    </p>
                    <p style="margin-right: 15pt;">39. To request confirmation of the name and city/state of
                        residence for the Prize
                        winner(s), please send a self- addressed, stamped business size envelope, within
                        2 days after the closing
                        date of the Promotion, to: Giveaway Corp Inc. 201 East 69th Street Ste. 10K New York, NY
                        10012. Late requests will not
                        be acknowledged or returned.
                    </p>
                    <p> 40.
                        Facebook, YouTube, Instagram, or Snapchat may be
                        used to advertise or promote the&nbsp;Promotion. By entering the
                        Promotion, entrants agree
                        that the Promotion is in no way sponsored, endorsed or administered by, or associated with
                        Facebook,
                        YouTube, Instagram, or Snapchat; and to release Facebook, YouTube, Instagram, or Snapchat
                        from all liability in relation to this Promotion. Any questions,
                        comments or complaints regarding the
                        Promotion should be directed to GAC and not Facebook, YouTube, Instagram, or Snapchat.
                    </p>
                    <p style="margin-right: 32pt;">41. All participants and entrants MUST
                        agree to the complete release
                        of liability of Facebook, YouTube, Instagram, or Snapchat.; as well as agree to acknowledge
                        that this promotion is in no way sponsored, endorsed or administered by, or associated with,
                        Facebook, YouTube,
                        Instagram, or Snapchat.
                    </p>
                    <p>42. If for any reason this competition is not capable of
                        running as planned, including due to infection by computer virus,
                        bugs, tampering, unauthorized intervention, fraud, technical failures or any
                        causes beyond the control of
                        GAC, which corrupt or affect the administration, security, fairness or
                        integrity or proper conduct of this promotion, GAC reserves the right in its
                        sole discretion to disqualify any individual who tampers with the entry
                        process, take any action that may be available, and to cancel,
                        terminate, modify or suspend the competition, subject
                        to any direction given under State Regulation with no liability to GAC.
                    </p>
                </div>
                <p style="margin-right: 30pt;">43. All participants, as a condition of participation in this
                    Promotion, agree to indemnify, release and hold harmless GAC and Released Parties from and
                    against any and
                    all liability, claims,
                    damages, injuries or actions of any kind
                    whatsoever for injuries, damages, or losses
                    to persons or property which
                    may be sustained in
                    connection with:
                </p>
                <p style="margin-left: 90px;">&nbsp; &nbsp;(a)
                    accessing GAC’s Website;
                </p>
                <p style="margin-right: 16pt; margin-left: 77pt;">(b)
                    participating
                    in any aspect of the Promotion, including, without limitation, while preparing for,
                    participating in and/or traveling to or from any prize or Promotion-related activity, including,
                    without limitation, any injury, damage,
                    death, loss or accident to person or property;
                </p>
                <p style="margin-left: 90px;">&nbsp; &nbsp;(c)
                    the receipt, ownership, possession, use or misuse of
                    any prize awarded;
                </p>
                <p style="margin-left: 90px;">&nbsp; &nbsp;(d)
                    viruses or any downloading or other problems
                    with the Promotion Application, or;
                </p>
                <p style="margin-right: 19pt; margin-left: 77pt;">(e)
                    any typographical or other error
                    in these T&Cs
                    or the announcement of offering
                    of any prize.
                </p>
                <p style="margin-right: 7pt;">44. GAC makes no representations or warranties of any kind
                    concerning the appearance, safety or performance of any prize, except
                    for any express
                    manufacturer's warranty as may be included with
                    the prize. Each
                    Prize Winner bears all liability and risk of loss or damage to his/her respective prize after it
                    has been delivered.
                </p>
                <p>45.
                    GAC’s decision is
                    final.
                </p>
                <p>46.
                    This promotion is void where
                    prohibited by law.
                </p>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<style>
    @media (max-width: 768px) {
        header {
            display: none !important;
        }
    }
</style>