<?php

/* @var $this \yii\web\View */

/* @var $content string */

\app\assets\UserAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->head() ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167190189-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-167190189-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5VQKSNG');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '605878313672331');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=605878313672331&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (m, e, t, r, i, k, a) {
            m[i] = m[i] || function () {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
        ym(62526682, "init", {clickmap: true, trackLinks: true, accurateTrackBounce: true, webvisor: true}); </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/62526682" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript> <!-- /Yandex.Metrika counter -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
    <title>GIVEAWAY GODS</title>
</head>
<body>
<?php $this->beginBody() ?>
<header class="header">
    <div class="header__left">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="99.83203125 142.0390625 412.16796875 143.625" version="1.1">
            <g>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 122.59375 163.824219 L 147.304688 163.824219 L 146.554688 191.769531 C 146.480469 194.246094 144.226563 194.398438 144.152344 194.398438 C 144.152344 194.398438 127.699219 195.523438 115.53125 195.523438 C 105.089844 195.523438 99.832031 188.761719 99.832031 181.253906 L 99.832031 156.3125 C 99.832031 148.800781 105.089844 142.039063 115.53125 142.039063 C 128.074219 142.039063 144.152344 142.714844 144.152344 142.714844 L 144.074219 155.183594 L 119.8125 155.183594 C 116.582031 155.183594 115.378906 156.914063 115.378906 159.46875 L 115.378906 178.097656 C 115.378906 180.574219 116.582031 182.378906 119.8125 182.378906 L 132.808594 182.304688 L 132.882813 175.769531 L 122.59375 175.015625 Z M 122.59375 163.824219 "></path>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 169.6875 195.074219 L 154.140625 195.074219 L 154.140625 142.492188 L 169.6875 142.492188 Z M 169.6875 195.074219 "></path>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 210.554688 142.488281 L 228.132813 142.488281 C 228.132813 142.488281 215.886719 187.789063 214.535156 192.820313 C 213.933594 195 213.105469 195.300781 211.90625 195.300781 L 189.96875 195.300781 C 188.769531 195.300781 188.015625 195 187.417969 192.820313 C 185.988281 187.789063 173.746094 142.488281 173.746094 142.488281 L 191.324219 142.488281 L 200.488281 179.824219 L 201.386719 179.824219 Z M 210.554688 142.488281 "></path>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 232.035156 142.488281 L 273.351563 142.488281 L 273.351563 155.035156 L 247.660156 155.035156 L 247.660156 162.097656 L 269.070313 162.097656 L 269.070313 175.015625 L 247.660156 175.015625 L 247.660156 181.851563 L 273.199219 181.851563 L 273.199219 195.074219 L 232.035156 195.074219 Z M 232.035156 142.488281 "></path>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 300.316406 171.335938 L 311.433594 171.335938 L 306.777344 154.285156 L 304.972656 154.285156 Z M 314.664063 184.105469 L 297.085938 184.105469 L 294.082031 195.074219 L 277.253906 195.074219 C 277.253906 195.074219 291.902344 148.574219 293.105469 144.820313 C 293.78125 142.566406 294.007813 142.488281 297.085938 142.488281 L 314.589844 142.488281 C 317.667969 142.488281 317.894531 142.566406 318.644531 144.820313 C 319.847656 148.574219 334.421875 195.074219 334.421875 195.074219 L 317.667969 195.074219 Z M 314.664063 184.105469 "></path>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 391.210938 142.488281 L 408.785156 142.488281 C 408.785156 142.488281 399.921875 188.464844 399.019531 193.269531 C 398.722656 195 397.820313 195.300781 396.691406 195.300781 L 376.5625 195.300781 C 375.660156 195.300781 374.457031 195.074219 374.230469 193.570313 C 373.707031 190.417969 370.628906 167.203125 370.628906 167.203125 L 370.324219 167.203125 C 370.324219 167.203125 367.246094 190.417969 366.722656 193.570313 C 366.496094 195.074219 365.292969 195.300781 364.390625 195.300781 L 344.261719 195.300781 C 343.132813 195.300781 342.230469 195 341.933594 193.269531 C 341.03125 188.464844 332.164063 142.488281 332.164063 142.488281 L 349.742188 142.488281 L 355.078125 179.898438 L 355.679688 179.898438 L 361.460938 142.488281 L 379.492188 142.488281 L 385.273438 179.898438 L 385.875 179.898438 Z M 391.210938 142.488281 "></path>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 429.519531 171.335938 L 440.636719 171.335938 L 435.980469 154.285156 L 434.175781 154.285156 Z M 443.867188 184.105469 L 426.289063 184.105469 L 423.285156 195.074219 L 406.457031 195.074219 C 406.457031 195.074219 421.105469 148.574219 422.304688 144.820313 C 422.984375 142.566406 423.207031 142.488281 426.289063 142.488281 L 443.789063 142.488281 C 446.871094 142.488281 447.09375 142.566406 447.847656 144.820313 C 449.046875 148.574219 463.621094 195.074219 463.621094 195.074219 L 446.871094 195.074219 Z M 443.867188 184.105469 "></path>
                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 483.601563 161.796875 L 494.195313 142.488281 L 512 142.488281 L 491.492188 179.75 L 491.492188 195.074219 L 475.867188 195.074219 L 475.867188 179.976563 L 455.207031 142.488281 L 473.011719 142.488281 Z M 483.601563 161.796875 "></path>
                <path xmlns="http://www.w3.org/2000/svg"
                      style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 125.390625 254.011719 L 150.269531 254.011719 L 149.515625 282.144531 C 149.4375 284.636719 147.167969 284.789063 147.09375 284.789063 C 147.09375 284.789063 130.53125 285.925781 118.28125 285.925781 C 107.769531 285.925781 102.476563 279.117188 102.476563 271.554688 L 102.476563 246.449219 C 102.476563 238.886719 107.769531 232.082031 118.28125 232.082031 C 130.910156 232.082031 147.09375 232.761719 147.09375 232.761719 L 147.019531 245.316406 L 122.59375 245.316406 C 119.339844 245.316406 118.128906 247.054688 118.128906 249.625 L 118.128906 268.378906 C 118.128906 270.875 119.339844 272.691406 122.59375 272.691406 L 135.675781 272.613281 L 135.75 266.035156 L 125.390625 265.277344 Z M 125.390625 254.011719 "/>
                <path xmlns="http://www.w3.org/2000/svg"
                      style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 171.371094 249.246094 L 171.371094 268.53125 C 171.371094 271.101563 172.507813 272.839844 175.832031 272.839844 L 184.757813 272.839844 C 188.011719 272.839844 189.21875 271.101563 189.21875 268.53125 L 189.21875 249.246094 C 189.21875 246.675781 188.011719 245.085938 184.757813 245.085938 L 175.832031 245.085938 C 172.507813 245.085938 171.371094 246.675781 171.371094 249.246094 M 155.644531 271.328125 L 155.644531 246.675781 C 155.644531 239.039063 160.9375 232.457031 171.449219 232.457031 L 189.070313 232.457031 C 199.578125 232.457031 204.949219 239.039063 204.949219 246.675781 L 204.949219 271.328125 C 204.949219 278.890625 199.578125 285.546875 189.070313 285.546875 L 171.449219 285.546875 C 160.9375 285.546875 155.644531 278.890625 155.644531 271.328125 "/>

                <path xmlns="http://www.w3.org/2000/svg"
                      style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 244.65625 249.699219 C 244.65625 246.601563 243.140625 245.085938 239.890625 245.085938 L 227.414063 245.085938 L 227.414063 272.917969 L 239.890625 272.917969 C 243.140625 272.917969 244.65625 271.40625 244.65625 268.304688 Z M 211.683594 232.535156 L 244.199219 232.535156 C 254.710938 232.535156 260.308594 239.1875 260.308594 246.75 L 260.308594 271.175781 C 260.308594 278.816406 254.710938 285.46875 244.199219 285.46875 L 211.683594 285.46875 Z M 211.683594 232.535156 "/>
                <path xmlns="http://www.w3.org/2000/svg"
                      style=" stroke:none;fill-rule:nonzero;fill:rgb(13.729858%,12.159729%,12.548828%);fill-opacity:1;"
                      d="M 306.066406 245.617188 L 281.5625 245.617188 C 279.976563 245.617188 279.671875 245.691406 279.671875 247.28125 L 279.671875 247.886719 C 279.671875 249.019531 280.503906 249.246094 282.09375 249.550781 C 282.09375 249.550781 297.140625 252.273438 298.046875 252.421875 C 307.878906 254.238281 309.542969 258.09375 309.542969 265.808594 L 309.542969 271.859375 C 309.542969 281.3125 305.15625 285.925781 291.242188 285.925781 C 281.941406 285.925781 264.167969 284.5625 264.167969 284.5625 L 264.246094 272.3125 L 289.203125 272.3125 C 292.453125 272.3125 293.738281 271.632813 293.738281 269.363281 L 293.738281 267.851563 C 293.738281 267.167969 293.664063 266.261719 292.378906 266.035156 C 291.46875 265.808594 279.976563 263.765625 279.371094 263.691406 C 268.632813 261.800781 263.566406 261.121094 263.566406 250.984375 L 263.566406 245.996094 C 263.566406 236.617188 268.253906 232.082031 282.167969 232.082031 C 291.46875 232.082031 306.140625 233.441406 306.140625 233.441406 Z M 306.066406 245.617188 "/>
            </g>
        </svg>
        <p class="header__text">
            <b>What do we do?</b> We are a leading company that helps brands, influencers, and people gain THOUSANDS of
            real followers!
        </p>
        <div class="header__center">
                    <span class="header__price">
                        From <b>$990</b>
                    </span>
            <span class="header__next">
                        <a href="#next">
                        Next Giveaway
                        </a>
                    </span>
        </div>
        <p class="header__text header__text--2">
            <b>How does it work?</b> We host giveaways with top Instagram influencers that have 1 million+ followers. We
            then choose amazing prizes like home gyms, luxury bags, Apple products, cash, and more. The cool prizes
            attract the influencer’s large audience to participate in the giveaway and we direct them to follow your
            Instagram account as well as the other sponsor accounts to be eligible to win the prizes. This is 100%
            Instagram safe, there are no bots, and no catches. It’s a simple process with great results!
        </p>
    </div>
    <div class="header__right">
        <button class="button__header js-popup" data-popup="popup-consultate">
            Learn more
        </button>
        <p class="text text--2">
            Gain thousands of new Instagram followers by becoming a sponsor of a giveaway hosted by Giveaway Gods!
        </p>
        <div class="header__logo--inst">
            <img src="/img/3d.png" alt="logo"/>
        </div>
        <div class="header__logo--people">
            <img src="/img/people.png" alt=""/>
        </div>
        <h3 class="header__text--sub">
            Followers
        </h3>
    </div>
</header>
<?= $content ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a onclick="fbq('track', 'Contact')" href="https://wa.me/15166331596" class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
</a>
<?php
if (!isset($_COOKIE['cookie_agree'])) {
    ?>
    <footer>
        <div class="cookies">
            <div class="cookies__text">
                By continuing to use this site, you give us consent to use cookies. You may access our web data <a
                        href="<?= \yii\helpers\Url::to(['/main/privacy-policy']) ?>">privacy policy</a> for further
                details.
            </div>
            <button class="cookies__button js-cookie-agree">Ok</button>
        </div>
    </footer>
    <?php
}
?>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>
    AOS.init();
</script>
<?php $this->endBody() ?>
</body>
</html>
<style>
    .cookies {
        background: #8991FE;
        border: 1px solid black;
        padding: 15px 20px;
        position: fixed;
        bottom: 20px;
        left: 50%;
        transform: translate(-50%, 0);
        display: flex;
        line-height: 1.3;
        color: #fff;
        max-width: 100%;
        z-index: 100;
        width: 720px;
        font-family: Harmonia;
    }

    .cookies__text {
        margin-right: 20px;
        font-family: Harmonia;
        font-size: 16px;
    }

    .cookies__text a {
        color: #fff;
    }

    .cookies__button {
        background: #fff;
        border: none;
        margin: 0;
        padding: 10px 20px;
        cursor: pointer;
        font-size: 14px;
        text-transform: uppercase;
    }

    .cookies__button:focus {
        outline: none;
    }

    @media (max-width: 750px) {
        .cookies {
            width: 100%;
            left: 0;
            margin: 0;
            transform: none;
            bottom: 0;
            box-sizing: border-box;
            align-items: center;
        }
    }
</style>
<?php $this->endPage() ?>
