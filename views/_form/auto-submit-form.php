<?php
/**
 * @var $this \yii\web\View
 * @var $user \app\models\User
 * @var $url string
 * @var $shopId string
 * @var $scid string
 * @var $cost string
 * @var $paymentType string
 */
?>
<form method="POST" id="auto_submit_form" class="clearfix payment_form" action="<?= $url ?>">
    <input name="shopId" value="<?= $shopId ?>" type="hidden"/>
    <input name="scid" value="<?= $scid ?>" type="hidden"/>
    <input name="sum" value="<?= $cost ?>" type="hidden">
    <input name="customerNumber" value="<?= $user->id ?>" type="hidden"/>
    <input name="paymentType" value="<?= $paymentType ? $paymentType : '' ?>" type="hidden">
    <input name="cps_email" value="<?= $user->email ?>" type="hidden">
    <input name="cps_phone" value="<?= $user->phone ?>" type="hidden">
    <input name="custom_field" value="<?= $give_id ?>" type="hidden">
    <input name="shopSuccessURL" value="http://givecorp.ru/main/gives?pay=success&user_id=<?= $user->id ?>"
           type="hidden">
    <input name="shopFailURL" value="http://givecorp.ru/main/gives?pay=fail&user_id=<?= $user->id ?>" type="hidden">
</form>
<script type="text/javascript">
    document.forms[0].submit();
</script>