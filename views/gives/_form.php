<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gives */
/* @var $form yii\widgets\ActiveForm */
/* @var $blogers \app\models\Blogers[] */

?>

<div class="gives-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'bloger_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $blogers,
        'language' => 'ru',
        'options' => ['placeholder' => 'Выбрать блогера'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'subscribes_in')->textInput() ?>
    <?= $form->field($model, 'cost')->textInput() ?>
    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, 'sort')->textInput();
    }
    if ($model->image) {
        echo Html::img('/img/' . $model->image, ['class' => 'img_profile', 'style' => 'width: 200px']);
    }
    ?>

    <?= $form->field($model, 'date_start')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
