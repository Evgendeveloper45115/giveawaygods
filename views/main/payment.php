<?php
/**
 * @var $give \app\models\Gives
 */
?>
<section class="section_signup_custom">
    <div class="section_signup_inner">
        <div class="section_signup_header"><a class="logo logo_black" href="/">GIVECORP</a><a
                    class="header_menu menu_opener" href="#"><span></span><span
                        class="header_menu_middle"></span><span></span></a></div>
        <div class="section_signup_content_custom">
            <h1>Оплата <?= $give->title ?></h1>
            <?php $form = \yii\widgets\ActiveForm::begin([
                'id' => 'form-signsup',
                'validateOnChange' => true,
                'validateOnSubmit' => true,
                'enableClientValidation' => true,
                'options' => [
                    'class' => 'section_signup_form',
                ],
                'fieldConfig' => [
                    'template' => "<div class='section_signup_form_group section_signup_form_focused'>{label}{input}{error}</div>",
                ],
            ]);
            ?>
            <?= $form->field($user, 'first_name')->textInput()->label('Имя', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($user, 'last_name')->textInput()->label('Фамилия', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($user, 'patronymic')->textInput()->label('Отчество', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($user, 'phone')->textInput()->label('Телефон', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($user, 'email')->textInput()->label('email', ['class' => 'section_signup_form_title']) ?>
            <?php
            if (Yii::$app->user->isGuest) {
                echo $form->field($user, 'instagram_ak')->textInput()->label('Инстаграмм', ['class' => 'section_signup_form_title']);
                echo $form->field($user, 'password')->passwordInput()->label('Пароль(создастся учетная запись)', ['class' => 'section_signup_form_title']);
            }
            ?>
            <div class="pay-list">
                <div class="pay_title" style="margin-top: 40px;">Способ оплаты</div>
                <div class="pay_list"><label class="pay_item"><input type="radio" name="pay" value="card" checked>
                        <div class="pay__item">
                            <div class="pay_item_img" style="background-image: url(/img/card.png)"></div>
                            <div class="pay_item_name">Оплата картой</div>
                        </div>
                    </label><label class="pay_item"><input type="radio" name="pay" value="qiwi">
                        <div class="pay__item">
                            <div class="pay_item_img" style="background-image: url(/img/qiwi.png)"></div>
                            <div class="pay_item_name">Qiwi</div>
                        </div>
                    </label><label class="pay_item"><input type="radio" name="pay" value="yamoney">
                        <div class="pay__item">
                            <div class="pay_item_img" style="background-image: url(/img/yamoney.png)"></div>
                            <div class="pay_item_name">Яндекс.Деньги</div>
                        </div>
                    </label><label class="pay_item"><input type="radio" name="pay" value="sber">
                        <div class="pay__item">
                            <div class="pay_item_img" style="background-image: url(/img/sber.png)"></div>
                            <div class="pay_item_name">Сбербанк Онлайн</div>
                        </div>
                    </label>
                    <!--<label class="pay_item"><input type="radio" name="pay" value="webmoney">
                        <div class="pay__item">
                            <div class="pay_item_img" style="background-image: url(/img/webmoney.png)"></div>
                            <div class="pay_item_name">Webmoney</div>
                        </div>
                    </label>
                    <label class="pay_item"><input type="radio" name="pay" value="paypal">
                        <div class="pay__item">
                            <div class="pay_item_img" style="background-image: url(/img/paypal.png)"></div>
                            <div class="pay_item_name">Paypal</div>
                        </div>
                    </label>-->
                </div>
            </div>
            <div class="pay_bottom">
                <div class="pay_bottom_title">К оплате</div>
                <div class="pay_bottom_cost"><?= $_GET['cost'] ?> Р</div>
            </div>
            <div class="pay_button"><?= yii\helpers\Html::submitButton('Оплатить', ['class' => 'button button_transparent_blue', 'onclick' => "ym(53344498, 'reachGoal', 'pay'); return true;"]) ?></div>
            <div class="section_video_text"
                 style="text-align: center;margin-top: 22.9px;margin-bottom:30px;font-size: 15px;">
                Оформляя заказ, я соглашаюсь с условиями <a style="text-decoration: underline;color: #0000aa"
                                                            target="_blank"
                                                            href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>">договора-
                    оферты</a>
            </div>

            <?php \yii\widgets\ActiveForm::end(); ?>
        </div>
    </div>
</section>
<footer class="main_page_footer">
    <div class="wrapper">
        <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

            <div class="copyright">GiveCorp © 2019</div>
            <div class="main_page_footer_button"><span
                        class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
            </div>
        </div>
        <div class="footer__links">

            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                соглашение</a>
            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика
                обработки
                персональных данных</a>
        </div>
    </div>
</footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>
