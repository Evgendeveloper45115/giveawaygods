<?php

use yii\widgets\ActiveForm;

/**@var $model \app\models\User */
?>

    <section class="section_signup">
        <div class="section_signup_inner">
            <div class="section_signup_header"><a class="logo logo_black" href="/">GIVECORP</a><a
                        class="header_menu menu_opener" href="#"><span></span><span
                            class="header_menu_middle"></span><span></span></a></div>
            <div class="section_signup_content section_signup_content_custom">
                <h1>Зарегистрируйтесь в системе, <br>чтобы получить доступ к<br>участию в лучших гивах </h1>
                <?php $form = ActiveForm::begin([
                    'id' => 'form-signsup',
                    'enableClientValidation' => true,
                    'validateOnType' => true,
                    'validateOnChange' => true,
                    'options' => [
                        'class' => 'section_signup_form',
                    ],
                    'fieldConfig' => [
                        'template' => "<div class='section_signup_form_group'>{label}{error}{input}</div>",
                    ],
                ]);
                ?>
                <?= $form->field($model, 'first_name')->textInput(['class' => 'section_signup_form_group'])->label('Имя', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'last_name')->textInput()->label('Фамилия', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'patronymic')->textInput()->label('Отчество', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'phone')->textInput()->label('Телефон', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'email')->textInput()->label('Email', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'instagram_ak')->textInput()->label('Какой аккаунт вы
                        планируете продвигать?', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'target')->textInput()->label('Для какой цели вы
                        планируете участвовать в гиве?', ['class' => 'section_signup_form_title']) ?>
                <div class="section_signup_form_button">
                    <?= yii\helpers\Html::submitButton('Подать заявку', ['class' => 'button button_transparent_blue', 'onclick' => "ym(53344498, 'reachGoal', 'subscribe2'); fbq('track', 'CompleteRegistration'); return true;"]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>
    <footer class="main_page_footer">
        <div class="wrapper">
            <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

                <div class="copyright">GiveCorp © 2019</div>
                <div class="main_page_footer_button"><span
                            class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
                </div>
            </div>
            <div class="footer__links">

                <a target="_blank" href="/main/agreement" class="footer__link">Пользовательское
                    соглашение</a>
                <a target="_blank" href="/main/politic" class="footer__link">Политика
                    обработки
                    персональных данных</a>
            </div>
        </div>
    </footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>