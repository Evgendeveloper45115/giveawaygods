<section class="section_ready">
    <header class="header_transparent">
        <div class="wrapper">
            <div class="header"><a class="header_logo logo logo_white" href="/">givecorp</a>
                <div class="header_button"><a class="button button_transparent_white getconsultation_opener" href="#"> Получить консультацию</a></div>
                <div class="header_right"><a class="header_login login_opener" href="#">Войти</a><a class="header_menu menu_opener" href="#"><span></span><span class="header_menu_middle"></span><span></span></a></div>
            </div>
        </div>
    </header>
    <div class="section_ready_content">
        <div class="wrapper">
            <h1>Полный комплекс услуг <br>по организации гивов</h1>
            <div class="ready_afterh1">от набора спонсоров до отправки призов</div>
            <div class="ready_title">Основатели компании <br>популярные инстаблогеры</div>
            <div class="ready_blogers">
                <div class="ready_bloger ready_bloger_left"><a href="https://www.instagram.com/ler_chek">@Ler_chek</a><span>1,7 млн подписчиков</span></div>
                <div class="ready_bloger ready_bloger_right"><a href="https://www.instagram.com/artem_chek">@Artem_chek</a><span>672 тыс подписчиков</span></div>
            </div>
        </div>
    </div>
</section>
<section class="section_ready_get">
    <div class="wrapper">
        <div class="ready_get">
            <h2>Получите <span class="colorblue">уникальное </span><br>предложение <br>по сотрудничеству с нами</h2><a class="link getconsultation_opener" href="#">Получить</a>
        </div>
    </div>
</section>
<section class="organization_section">
    <div class="wrapper">
        <h2 class="tac fz28 organization_section_h2"> Мы организуем гивы популярных блогеров</h2>
        <div class="organization_carousel_block">
            <div class="organization_carousel_block_like organization_carousel_block_like1"></div>
            <div class="organization_carousel_block_like organization_carousel_block_like2"></div>
            <div class="organization_carousel_block_like organization_carousel_block_like3"></div>
            <div class="organization_carousel_block_like organization_carousel_block_like4"></div>
            <div class="organization_carousel">
                <div class="organization_carousel_item">
                    <div class="organization_carousel_content" style="background-image: url(/img/organization_carousel_item1.png)"></div>
                </div>
                <div class="organization_carousel_item">
                    <div class="organization_carousel_content" style="background-image: url(/img/organization_carousel_item2.png)"></div>
                </div>
                <div class="organization_carousel_item">
                    <div class="organization_carousel_content" style="background-image: url(/img/organization_carousel_item3.png)"></div>
                </div>
                <div class="organization_carousel_item">
                    <div class="organization_carousel_content" style="background-image: url(/img/organization_carousel_item4.png)"></div>
                </div>
                <div class="organization_carousel_item">
                    <div class="organization_carousel_content" style="background-image: url(/img/organization_carousel_item5.png)"></div>
                </div>
            </div>
        </div>
        <div class="organization_list">
            <div class="organization_item">
                <div class="organization_item_inner">
                    <div class="organization_item_icon" style="background-image: url(/img/organization_item_icon1.png)"></div>
                    <div class="organization_item_title">Собираем спонсоров гивов за вас</div>
                    <div class="organization_item_text">Более 1 500 спонсоров участвовавших в наших гивах</div>
                </div>
            </div>
            <div class="organization_item">
                <div class="organization_item_inner">
                    <div class="organization_item_icon" style="background-image: url(/img/organization_item_icon2.png)"></div>
                    <div class="organization_item_title">Организуем съемки промороликов для гива</div>
                    <div class="organization_item_text">Снимаем и монтируем ролик для старта гива</div>
                </div>
            </div>
            <div class="organization_item">
                <div class="organization_item_inner">
                    <div class="organization_item_icon" style="background-image: url(/img/organization_item_icon3.png)"></div>
                    <div class="organization_item_title">Проведение эфиров с лайктаймами</div>
                    <div class="organization_item_text">Проводим лайктайм стимулирующие активность</div>
                </div>
            </div>
            <div class="organization_item">
                <div class="organization_item_inner">
                    <div class="organization_item_icon" style="background-image: url(/img/organization_item_icon4.png)"></div>
                    <div class="organization_item_title">Отправка призов нашими силами и под нашу ответственность</div>
                    <div class="organization_item_text">Мы гарантируем честную и быструю отправку призов победителям</div>
                </div>
            </div>
            <div class="organization_item">
                <div class="organization_item_inner">
                    <div class="organization_item_icon" style="background-image: url(/img/organization_item_icon5.png)"></div>
                    <div class="organization_item_title">Проведение различных активностей на конкурсных аккаунтах</div>
                    <div class="organization_item_text">Обеспечиваем стабильную подписку на спонсорские аккаунты</div>
                </div>
            </div>
        </div>
        <div class="ready_get2">
            <h2 class="fz28">Получите <span class="colorblue">уникальное </span>предложение <br>по сотрудничеству с нами</h2><a class="link getconsultation_opener" href="#">Получить</a>
        </div>
    </div>
</section>
<footer class="main_page_footer">
    <div class="wrapper">
        <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>
            
            <div class="copyright">GiveCorp © 2019</div>
            <div class="main_page_footer_button"><span
                        class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
            </div>
        </div>
        <div class="footer__links">
        
            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                    соглашение</a>
                <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика обработки
                персональных данных</a>
        </div>
    </div>
</footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>