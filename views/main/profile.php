<?php

use yii\widgets\ActiveForm;

/**@var $model \app\models\User */
?>
    <div class="row">
        <div class="col-md-12">
            <section class="section_signup">
                <div class="section_signup_inner">
                    <div class="section_signup_header"><a class="logo logo_black"
                                                          href="<?= \yii\helpers\Url::to(['/main/gives']) ?>">GIVECORP</a><a
                                class="header_menu menu_opener" href="#"><span></span><span
                                    class="header_menu_middle"></span><span></span></a></div>
                    <div class="section_signup_content" style="padding-bottom: 0">
                        <h1>Данные клиента: <?= $model->first_name . ' ' . $model->last_name ?> </h1>
                        <div class="form-group field-user-first_name required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-first_name">Имя</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->first_name ?></div>
                            </div>
                        </div>
                        <div class="form-group field-user-last_name required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-last_name">Фамилия</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->last_name ?></div>
                            </div>
                        </div>
                        <div class="form-group field-user-patronymic required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-patronymic">Отчество</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->patronymic ?></div>

                            </div>
                        </div>
                        <div class="form-group field-user-phone required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-phone">Телефон</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->phone ?></div>
                            </div>
                        </div>
                        <div class="form-group field-user-email required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-email">Email</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->email ?></div>
                            </div>
                        </div>
                        <div class="form-group field-user-instagram_ak required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-instagram_ak">Какой аккаунт вы
                                    планируете продвигать?</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->instagram_ak ?></div>
                            </div>
                        </div>
                        <div class="form-group field-user-target required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-target">Для какой цели вы
                                    планируете участвовать в гиве?</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->target ?></div>
                            </div>
                        </div>
                        <div class="form-group field-user-password required">
                            <div class="section_signup_form_group section_signup_form_focused"><label
                                        class="section_signup_form_title" for="user-target">Пароль:</label>
                                <div style="border-bottom: 0.14706vw solid #e4e4e4;"><?= $model->password ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <footer class="main_page_footer">
        <div class="wrapper">
            <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

                <div class="copyright">GiveCorp © 2019</div>
                <div class="main_page_footer_button"><span
                            class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
                </div>
            </div>
            <div class="footer__links">

                <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                    соглашение</a>
                <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика обработки
                    персональных данных</a>
            </div>
        </div>
    </footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>