<?php
/**
 * @var $bloger \app\models\Blogers
 * @var $gives \app\models\Gives[]
 */
?>

<?= Yii::$app->controller->renderPartial('../blocks/header') ?>
    <section class="giveslate">
        <div class="wrapper">
            <h2>Проведенные гивы <br><?= $bloger->name ?> <?= $bloger->instagram_name ?></h2>
            <?php
            if (!empty($gives)) {
                ?>
                <div class="gives_list">
                    <?php
                    \yii\widgets\Pjax::begin()
                    ?>
                    <div class="giveslate_header">
                        <div class="sorting">
                            <a href="<?= \yii\helpers\Url::to(['/main/gives-completed', 'date' => $_GET['date'] == 'ASC' ? 'DESC' : 'ASC', 'bloger' => isset($_GET['bloger']) ? $_GET['bloger'] : false, 'limit' => $limit]) ?>">
                                <div class="sorting_item <?= isset($_GET['date']) ? 'active' : '' ?>">
                                    <div class="sorting_item_title">Дата старта</div>
                                    <div class="sorting_item_icons">
                                        <!--icon down-->
                                        <svg class="sorting_item_icon <?= $_GET['date'] == 'ASC' ? 'active' : '' ?>"
                                             viewBox="0 0 10 13">
                                            <path d="M9.098,8.751 C9.277,8.568 9.277,8.280 9.098,8.097 C8.912,7.920 8.614,7.920 8.435,8.097 L5.078,11.413 L5.078,0.468 C5.078,0.213 4.872,0.010 4.614,0.010 C4.356,0.010 4.144,0.213 4.144,0.468 L4.144,11.413 L0.793,8.097 C0.608,7.920 0.316,7.920 0.131,8.097 C-0.048,8.280 -0.048,8.568 0.131,8.751 L4.283,12.853 C4.462,13.030 4.760,13.030 4.946,12.853 L9.098,8.751 Z"></path>
                                        </svg>
                                        <!--icon up-->
                                        <svg class="sorting_item_icon <?= $_GET['date'] == 'DESC' ? 'active' : '' ?>"
                                             viewBox="0 0 10 13">
                                            <path d="M0.148,4.249 C-0.026,4.432 -0.026,4.720 0.148,4.903 C0.329,5.080 0.619,5.080 0.793,4.903 L4.059,1.586 L4.059,12.532 C4.059,12.786 4.259,12.989 4.510,12.989 C4.762,12.989 4.968,12.786 4.968,12.532 L4.968,1.586 L8.228,4.903 C8.409,5.080 8.692,5.080 8.873,4.903 C9.047,4.720 9.047,4.432 8.873,4.249 L4.832,0.147 C4.658,-0.030 4.368,-0.030 4.188,0.147 L0.148,4.249 Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </a>
                            <a href="<?= \yii\helpers\Url::to(['/main/gives-completed', 'cost' => $_GET['cost'] == 'ASC' ? 'DESC' : 'ASC', 'bloger' => isset($_GET['bloger']) ? $_GET['bloger'] : false, 'limit' => $limit]) ?>">
                                <div class="sorting_item <?= isset($_GET['cost']) ? 'active' : '' ?>">
                                    <div class="sorting_item_title">Стоимость</div>
                                    <div class="sorting_item_icons">
                                        <!--icon down-->
                                        <svg class="sorting_item_icon <?= $_GET['cost'] == 'ASC' ? 'active' : '' ?>"
                                             viewBox="0 0 10 13">
                                            <path d="M9.098,8.751 C9.277,8.568 9.277,8.280 9.098,8.097 C8.912,7.920 8.614,7.920 8.435,8.097 L5.078,11.413 L5.078,0.468 C5.078,0.213 4.872,0.010 4.614,0.010 C4.356,0.010 4.144,0.213 4.144,0.468 L4.144,11.413 L0.793,8.097 C0.608,7.920 0.316,7.920 0.131,8.097 C-0.048,8.280 -0.048,8.568 0.131,8.751 L4.283,12.853 C4.462,13.030 4.760,13.030 4.946,12.853 L9.098,8.751 Z"></path>
                                        </svg>
                                        <!--icon up-->
                                        <svg class="sorting_item_icon <?= $_GET['cost'] == 'DESC' ? 'active' : '' ?>"
                                             viewBox="0 0 10 13">
                                            <path d="M0.148,4.249 C-0.026,4.432 -0.026,4.720 0.148,4.903 C0.329,5.080 0.619,5.080 0.793,4.903 L4.059,1.586 L4.059,12.532 C4.059,12.786 4.259,12.989 4.510,12.989 C4.762,12.989 4.968,12.786 4.968,12.532 L4.968,1.586 L8.228,4.903 C8.409,5.080 8.692,5.080 8.873,4.903 C9.047,4.720 9.047,4.432 8.873,4.249 L4.832,0.147 C4.658,-0.030 4.368,-0.030 4.188,0.147 L0.148,4.249 Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                    foreach ($gives as $give) {
                        ?>
                        <div class="gives_item" video="<?= $give->video_link ?>">
                            <div class="gives_item_img" style="background: url(<?= $give->image ? '/img/' . $give->image : '/img/section_video_video.jpg' ?>) no-repeat center/cover;">
                                <div class="getconsultation_center_custom" style="position: relative">
                                    <div class="js-player"></div>
                                </div>
                                <div class="section_video_playbutton playvideo js-youtube"
                                     href="#<?= $give->video_link ?>">
                                    <svg viewBox="0 0 37 44">
                                        <defs>
                                            <lineargradient id="PSgrad_0" x1="0%" x2="42.262%" y1="90.631%" y2="0%">
                                                <stop offset="0%" stop-color="rgb(88,199,219)" stop-opacity="1"></stop>
                                                <stop offset="100%" stop-color="rgb(76,104,214)"
                                                      stop-opacity="1"></stop>
                                            </lineargradient>
                                        </defs>
                                        <path fill-rule="evenodd" fill="rgb(0, 0, 0)"
                                              d="M34.764,17.929 L7.780,0.767 C6.265,-0.196 4.254,-0.257 2.679,0.607 C1.104,1.470 -0.000,3.058 -0.000,4.851 L-0.000,39.148 C-0.000,40.941 1.104,42.529 2.678,43.392 C3.419,43.798 4.300,44.000 5.099,44.000 C5.998,44.000 6.934,43.743 7.737,43.234 L34.743,26.097 C36.149,25.204 36.999,23.678 37.000,22.013 C37.000,20.350 36.171,18.823 34.764,17.929 L34.764,17.929 ZM33.706,24.439 L6.720,41.576 C5.819,42.148 4.561,42.182 3.626,41.670 C2.690,41.158 1.968,40.216 1.968,39.149 L1.968,4.852 C1.968,3.787 2.690,2.844 3.626,2.332 C4.065,2.091 4.621,1.971 5.095,1.971 C5.629,1.971 6.203,2.123 6.680,2.426 L33.685,19.587 C34.520,20.118 35.028,21.024 35.027,22.013 C35.029,23.002 34.541,23.908 33.706,24.439 Z"></path>
                                        <path fill="url(#PSgrad_0)"
                                              d="M34.764,17.929 L7.780,0.767 C6.265,-0.196 4.254,-0.257 2.679,0.607 C1.104,1.470 -0.000,3.058 -0.000,4.851 L-0.000,39.148 C-0.000,40.941 1.104,42.529 2.678,43.392 C3.419,43.798 4.300,44.000 5.099,44.000 C5.998,44.000 6.934,43.743 7.737,43.234 L34.743,26.097 C36.149,25.204 36.999,23.678 37.000,22.013 C37.000,20.350 36.171,18.823 34.764,17.929 L34.764,17.929 ZM33.706,24.439 L6.720,41.576 C5.819,42.148 4.561,42.182 3.626,41.670 C2.690,41.158 1.968,40.216 1.968,39.149 L1.968,4.852 C1.968,3.787 2.690,2.844 3.626,2.332 C4.065,2.091 4.621,1.971 5.095,1.971 C5.629,1.971 6.203,2.123 6.680,2.426 L33.685,19.587 C34.520,20.118 35.028,21.024 35.027,22.013 C35.029,23.002 34.541,23.908 33.706,24.439 Z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div class="gives_item_content">
                                <div class="gives_item__content">
                                    <div class="gives_item_title">
                                        <span>Подписчиков пришло: </span><?= $give->count_sub ?>
                                    </div>
                                    <div class="gives_item_title2"><span>Стоимость подписчика: </span><?= $give->cost ?>
                                        руб.
                                    </div>
                                    <div class="gives_item_text">Подарки: <?= $give->presents ?></div>
                                    <a class="gives_item_link link" href="<?= \yii\helpers\Url::to(['/main/gives']) ?>">Участвовать
                                        в гиве</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    if ($display) {
                        ?>
                        <div class="gives_list_loadmore"><a class="button button_transparent_black"
                                                            href="<?= \yii\helpers\Url::to(['/main/gives-completed', 'date' => $date, 'cost' => $cost, 'bloger' => $bloger, 'limit' => $limit + 3]) ?>">Загрузить
                                еще</a>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    \yii\widgets\Pjax::end()
                    ?>
                </div>
                <?php
            } else {
                ?>
                <style>
                    footer {
                        position: absolute;
                        left: 0;
                        bottom: 0;
                        width: 100%;
                    }
                </style>

                <div style="font-weight: bold">У данного блогера еще не было проведенных гивов</div>
                <?php
            }
            ?>
        </div>
    </section>
    <footer class="main_page_footer">
        <div class="wrapper">
            <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

                <div class="copyright">GiveCorp © 2019</div>
                <div class="main_page_footer_button"><span
                            class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
                </div>
            </div>
            <div class="footer__links">

                <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                    соглашение</a>
                <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика
                    обработки
                    персональных данных</a>
            </div>
        </div>
    </footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>