<?php
/**
 * @var $promotions \app\models\Promotion[]
 */
?>
<section class="section_buy">
    <?= Yii::$app->controller->renderPartial('../blocks/header', ['header_transparent' => 'header_transparent']) ?>
    <div class="section_buy_body">
        <div class="wrapper">
            <div class="section_buy_title">Сервис оптовых закупок<br>рекламы в инстаграм</div>
            <div class="section_buy_text">Только эффективные блогеры и низкие цены</div>
            <div class="section_buy_button">
                <div id="button_bottom" class="button button_transparent_white" href="#">Смотреть список
                    блогеров
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_textbuy">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6">
                <h2>Высокая экспертность<br>закупок рекламы у блогеров<br>и низкие цены</h2>
            </div>
            <div class="col-md-6">
                <p>Мы постоянно покупаем рекламу для наших проектов, в том числе и для нашей косметики Letique, замеряем
                    ее эффективность, поэтому имеем точное представление об эффективности рекламы у каждого конкретного
                    блогера, из-за этого в нашей базе только качественные блогеры с реальной активной и платежеспособной
                    аудиторией. Мы отобрали для вас лучших.</p>
            </div>
        </div>
    </div>
</section>
<section class="section_advantages">
    <div class="wrapper">
        <h2>Преимущества</h2>
        <div class="advantages">
            <div class="advantage">
                <div class="advantage_inner">
                    <div class="advantage_icon" style="background-image: url(/img/advantage_icon5.png)"></div>
                    <div class="advantage_text">Подбор блогеров <span
                                class="colorblue">под разные целевые аудитории </span>для эффективной рекламы
                    </div>
                </div>
            </div>
            <div class="advantage">
                <div class="advantage_inner">
                    <div class="advantage_icon" style="background-image: url(/img/advantage_icon6.png)"></div>
                    <div class="advantage_text">В нашей базе только качественые блогеры с <span class="colorblue">живой аудиторией </span>
                    </div>
                </div>
            </div>
            <div class="advantage">
                <div class="advantage_inner">
                    <div class="advantage_icon" style="background-image: url(/img/advantage_icon7.png)"></div>
                    <div class="advantage_text"><span class="colorblue">Более 50 блогеров, </span>готовых сделать
                        рекламу для вас по привлекательной цене
                    </div>
                </div>
            </div>
            <div class="advantage">
                <div class="advantage_inner">
                    <div class="advantage_icon" style="background-image: url(/img/advantage_icon8.png)"></div>
                    <div class="advantage_text"><span class="colorblue">Охват более 90 млн </span><br>аудитории
                        инстаграм
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_stars">
    <div class="wrapper">
        <h2>Мы сотрудничаем <br>с топовыми блогерами</h2>
        <?php \yii\widgets\Pjax::begin([
            'id' => 'containerId', //контеинер
            'clientOptions' => ['method' => 'POST'], //тип запроса
            'enablePushState' => FALSE, //обновлять url
            'timeout' => 3000, //время выполнения запроса
            'linkSelector' => '.main_custom', //обрабатывать через pjax только отдельные ссылки
        ]); ?>

        <div class="stars" id="containerId">
            <?php
            foreach ($blogers as $bloger) {
                if ($bloger->is_star_promotion) {
                    ?>
                    <div class="star">
                        <div class="star_photo" style="background-image: url(/img/<?= $bloger->avatar ?>)"></div>
                        <div class="star_name"><?= $bloger->name ?></div>
                        <a class="star_instagramm" href="<?= $bloger->instagram ?>"
                           target="_blank"><?= $bloger->instagram_name ?></a>
                        <div class="star_subscribers"><?= $bloger->count_subscribes ?></div>
                        <?php
                        if (Yii::$app->user->isGuest) {
                            ?>
                            <a class="star_link" href="<?= \yii\helpers\Url::to(['/main/registration']) ?>">
                                <span>Купить рекламу</span></a>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
            }
            ?>

        </div>
        <div class="stars_carousel_buttons">
            <div class="stars_carousel_button stars_carousel_button_prev">
                <svg viewBox="0 0 30 14">
                    <path d="M0.229,6.429 L6.224,0.236 C6.528,-0.079 7.017,-0.079 7.321,0.236 C7.626,0.551 7.626,1.056 7.321,1.371 L2.653,6.199 L29.220,6.199 C29.651,6.199 30.000,6.560 30.000,7.006 C30.000,7.452 29.651,7.813 29.220,7.813 L2.653,7.813 L7.321,12.629 C7.626,12.944 7.626,13.449 7.321,13.764 C7.169,13.921 6.973,14.000 6.770,14.000 C6.567,14.000 6.370,13.921 6.218,13.764 L0.223,7.564 C-0.075,7.256 -0.075,6.744 0.229,6.429 Z"></path>
                </svg>
            </div>
            <div class="stars_carousel_button stars_carousel_button_next">
                <svg viewBox="0 0 30 14">
                    <path d="M29.771,6.429 L23.776,0.236 C23.471,-0.079 22.983,-0.079 22.679,0.236 C22.374,0.551 22.374,1.056 22.679,1.371 L27.347,6.199 L0.780,6.199 C0.349,6.199 -0.000,6.560 -0.000,7.006 C-0.000,7.452 0.349,7.813 0.780,7.813 L27.347,7.813 L22.679,12.629 C22.374,12.944 22.374,13.449 22.679,13.764 C22.831,13.921 23.027,14.000 23.230,14.000 C23.433,14.000 23.630,13.921 23.782,13.764 L29.777,7.564 C30.075,7.256 30.075,6.744 29.771,6.429 Z"></path>
                </svg>
            </div>
        </div>
        <?php
        if (Yii::$app->devicedetect->isMobile() && $display) {
            ?>
            <div style="text-align: center">
                <div class="gives_list_loadmore"><a class="main_custom button button_transparent_black"
                                                    href="<?= \yii\helpers\Url::to(['/main/promotion', 'limit' => $limit + 4]) ?>">Загрузить
                        еще</a>
                </div>

            </div>
            <?php
        }
        ?>

        <?php
        \yii\widgets\Pjax::end()
        ?>
    </div>
</section>
<?php
\yii\widgets\Pjax::begin(['id' => 'containerId2']);
?>
<section class="section_givs" id="gives">
    <div class="wrapper" id="elementtoScrollToID">
        <h2>Совместные закупки рекламы</h2>
        <div class="giveslate_header">
            <div class="sorting">
                <a href="<?= \yii\helpers\Url::to(['/main/promotion', 'cost' => $_GET['cost'] == 'ASC' ? 'DESC' : 'ASC']) ?>">
                    <div class="sorting_item <?= isset($_GET['cost']) ? 'active' : '' ?>">
                        <div class="sorting_item_title">Цена</div>
                        <div class="sorting_item_icons">
                            <!--icon down-->
                            <svg class="sorting_item_icon <?= $_GET['cost'] == 'ASC' ? 'active' : '' ?>"
                                 viewBox="0 0 10 13">
                                <path d="M9.098,8.751 C9.277,8.568 9.277,8.280 9.098,8.097 C8.912,7.920 8.614,7.920 8.435,8.097 L5.078,11.413 L5.078,0.468 C5.078,0.213 4.872,0.010 4.614,0.010 C4.356,0.010 4.144,0.213 4.144,0.468 L4.144,11.413 L0.793,8.097 C0.608,7.920 0.316,7.920 0.131,8.097 C-0.048,8.280 -0.048,8.568 0.131,8.751 L4.283,12.853 C4.462,13.030 4.760,13.030 4.946,12.853 L9.098,8.751 Z"></path>
                            </svg>
                            <!--icon up-->
                            <svg class="sorting_item_icon <?= $_GET['cost'] == 'DESC' ? 'active' : '' ?>"
                                 viewBox="0 0 10 13">
                                <path d="M0.148,4.249 C-0.026,4.432 -0.026,4.720 0.148,4.903 C0.329,5.080 0.619,5.080 0.793,4.903 L4.059,1.586 L4.059,12.532 C4.059,12.786 4.259,12.989 4.510,12.989 C4.762,12.989 4.968,12.786 4.968,12.532 L4.968,1.586 L8.228,4.903 C8.409,5.080 8.692,5.080 8.873,4.903 C9.047,4.720 9.047,4.432 8.873,4.249 L4.832,0.147 C4.658,-0.030 4.368,-0.030 4.188,0.147 L0.148,4.249 Z"></path>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="<?= \yii\helpers\Url::to(['/main/promotion', 'count_seat' => $_GET['count_seat'] == 'ASC' ? 'DESC' : 'ASC']) ?>">
                    <div class="sorting_item <?= isset($_GET['count_seat']) ? 'active' : '' ?>">
                        <div class="sorting_item_title">К-тво мест</div>
                        <div class="sorting_item_icons">
                            <!--icon down-->
                            <svg class="sorting_item_icon <?= $_GET['count_seat'] == 'ASC' ? 'active' : '' ?>"
                                 viewBox="0 0 10 13">
                                <path d="M9.098,8.751 C9.277,8.568 9.277,8.280 9.098,8.097 C8.912,7.920 8.614,7.920 8.435,8.097 L5.078,11.413 L5.078,0.468 C5.078,0.213 4.872,0.010 4.614,0.010 C4.356,0.010 4.144,0.213 4.144,0.468 L4.144,11.413 L0.793,8.097 C0.608,7.920 0.316,7.920 0.131,8.097 C-0.048,8.280 -0.048,8.568 0.131,8.751 L4.283,12.853 C4.462,13.030 4.760,13.030 4.946,12.853 L9.098,8.751 Z"></path>
                            </svg>
                            <!--icon up-->
                            <svg class="sorting_item_icon <?= $_GET['count_seat'] == 'DESC' ? 'active' : '' ?>"
                                 viewBox="0 0 10 13">
                                <path d="M0.148,4.249 C-0.026,4.432 -0.026,4.720 0.148,4.903 C0.329,5.080 0.619,5.080 0.793,4.903 L4.059,1.586 L4.059,12.532 C4.059,12.786 4.259,12.989 4.510,12.989 C4.762,12.989 4.968,12.786 4.968,12.532 L4.968,1.586 L8.228,4.903 C8.409,5.080 8.692,5.080 8.873,4.903 C9.047,4.720 9.047,4.432 8.873,4.249 L4.832,0.147 C4.658,-0.030 4.368,-0.030 4.188,0.147 L0.148,4.249 Z"></path>
                            </svg>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <table class="section_givs_table">
            <thead>
            <tr>
                <th>Блогер</th>
                <th class="tac">Подписчиков</th>
                <th class="tac">Аудитория</th>
                <th class="tac">Интересы</th>
                <th class="tac">Мест</th>
                <th>Стоимость</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($promotions as $promotion) {
                $PHU = \app\models\PromotionHasUser::findOne(['promotion_id' => $promotion->id, 'user_id' => Yii::$app->user->id])
                ?>
                <tr>
                    <td class="mobile100">
                        <div class="posrel">
                            <div class="section_givs_bloger table_popup_bloger_opener">
                                <div class="section_givs_bloger_photo"
                                     style="background-image: url(/img/<?= $promotion->bloger->avatar ?>)"></div>
                                <div class="section_givs_bloger_instagramm"><?= $promotion->bloger->instagram_name ?>
                                    <div class="section_givs_table_subMobile"><?= $promotion->bloger->count_subscribes ?></div>
                                </div>
                                <div class="section_givs_table_text">Полная статистика</div>
                            </div>
                            <div class="table_popup table_popup_top table_popup_bloger"><span class="table_popup_close"><svg
                                            viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" fill="rgb(77, 115, 215)"
                                                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.518,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.480,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
                                        </svg></span>
                                <div class="table_popup_bloger_photo"
                                     style="background-image: url(/img/<?= $promotion->bloger->avatar ?>)"></div>
                                <div class="table_popup_bloger_name"><?= $promotion->bloger->name ?></div>
                                <a class="table_popup_bloger_inst"
                                   href="<?= $promotion->bloger->instagram ?>"><?= $promotion->bloger->instagram_name ?></a>
                                <div class=" table_popup_bloger_sub"><?= $promotion->bloger->count_subscribes ?>
                                    подписчиков
                                </div>
                                <div class="table_popup_bloger_text"><?= $promotion->bloger->text ?>
                                </div>
                                <hr>
                                <?php
                                if ($promotion->bloger->statistics_img) {
                                    ?>
                                    <h1>Статистика</h1>

                                    <span style="display: inline-flex;width: 50%;margin-top: 10px">
                                    <span class="hover one_one table_popup_bloger_text"
                                          style="text-decoration: underline">Возраст</span>
                                    <span class="hover sec_sec table_popup_bloger_text"
                                          style="margin-left: 40%">Гео</span>
                                </span>
                                    <div class="table_popup_bloger_bottom" style="background: white;overflow: scroll; height: 220px;z-index: 999;">
                                        <div class="one" style="display: block">
                                            <img src="/img/<?= $promotion->bloger->statistics_img ?>">
                                        </div>
                                        <div class="sec" style="display: none">
                                            <img src="/img/<?= $promotion->bloger->statistics_img_two ?>">
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </td>
                    <td class="tac section_givs_table_nomobile">
                        <div class="posrel">
                            <div class="mobile_th"> Подписчиков</div>
                            <div class="table_popup_warranty_opener">
                                <div class="section_givs_table_title"><?= $promotion->bloger->count_subscribes ?></div>
                            </div>
                        </div>
                    </td>
                    <td class="tac section_givs_table40">
                        <div class="posrel">
                            <div class="mobile_th"> Аудитория</div>
                            <div class="table_popup_warranty_opener">
                                <div class="section_givs_table_title"><?= $promotion->auditory ?></div>
                            </div>
                        </div>
                    </td>
                    <td class="tac section_givs_table40">
                        <div class="posrel">
                            <div class="mobile_th"> Интересы</div>
                            <div class="table_popup_warranty_opener">
                                <div class="section_givs_table_title"><?= $promotion->interest ?></div>
                            </div>
                        </div>
                    </td>
                    <td class="tac section_givs_table20">
                        <div class="mobile_th"> Мест</div>
                        <div class="section_givs_table_title"
                             style="white-space: nowrap">
                            <?= trim(($promotion->plus_subscriber ?
                                count($promotion->promotionHasUsers) + $promotion->plus_subscriber :
                                count($promotion->promotionHasUsers))) ?>/<?= $promotion->count_seat ?>
                        </div>
                    </td>
                    <td class="mobile100">
                        <div class="section_givs_table_cost">
                            <div
                                    class="section_givs_table__cost section_givs_table_title"><?= Yii::$app->user->isGuest ? '<a href="/main/registration/">Узнать цену</a>' : $promotion->all_cost . ' руб.' ?> </div>
                            <?php
                            if (!$PHU) {
                                ?>
                                <a class="link"
                                   href="<?= \yii\helpers\Url::to(Yii::$app->user->isGuest ? '/main/registration' : \yii\helpers\Url::to(['/main/pre-pay', 'cost' => $promotion->all_cost, 'give_id' => $promotion->id])) ?>"><?= "Купить" ?></a>
                                <?php
                            } else {
                                ?>
                                <div class="link">Оплачено</div>
                                <?php
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</section>
<?php
\yii\widgets\Pjax::end();
?>

<footer class="main_page_footer" style="margin-top: 15px">
    <div class="wrapper">
        <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

            <div class="copyright">GiveCorp © 2019</div>
            <div class="main_page_footer_button"><span
                        class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
            </div>
        </div>
        <div class="footer__links">

            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                соглашение</a>
            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика
                обработки
                персональных данных</a>
        </div>
    </div>
</footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>
