<?php
/**
 * @var $this \yii\web\View
 */
?>
    <section class="section_signup">
        <div class="section_signup_inner">
            <div class="section_signup_header"><a class="logo logo_black" href="/">GIVECORP</a><a
                        class="header_menu menu_opener" href="#"><span></span><span
                            class="header_menu_middle"></span><span></span></a></div>
            <div class="section_signup_content">
                <h1>Вы успешно подали заявку</h1>
                <div class="section_signup_form">
                    <div class="section_signup_successtext tac">
                        <p>Вашу заявку рассматривает менеджер.</p>
                        <p>После одобрения заявки на указанный email будет выслан логин и пароль для входа в личный
                            кабинет.</p>
                    </div>
                    <div class="section_signup_form_button"><a class="custom__signup_form_button button button_transparent_blue" href="/">Перейти
                            на главную</a>
                    </div>
                </div>
            </div>
       </div>
    </section>
    <footer class="main_page_footer">
        <div class="wrapper">
            <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

                <div class="copyright">GiveCorp © 2019</div>
                <div class="main_page_footer_button"><span class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
                </div>
            </div>
            <div class="footer__links">

                <a target="_blank" href="/main/agreement" class="footer__link">Пользовательское
                    соглашение</a>
                <a target="_blank" href="/main/politic" class="footer__link">Политика обработки
                    персональных данных</a>
            </div>
        </div>
    </footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>