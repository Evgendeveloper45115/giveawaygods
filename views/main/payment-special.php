<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 15.04.2019
 * Time: 12:21
 */
?>
<section class="section_signup">
    <div class="section_signup_inner">
        <div class="section_signup_header"><a class="logo logo_black" href="/">GIVECORP</a><a
                    class="header_menu menu_opener" href="#"><span></span><span
                        class="header_menu_middle"></span><span></span></a></div>
        <div class="section_signup_content section_signup_content_custom">
            <h1>Заполните данные для выставления счета</h1>
            <?php $form = \yii\widgets\ActiveForm::begin([
                'id' => 'form-signsup',
                'enableClientValidation' => true,
                'validateOnType' => true,
                'validateOnChange' => true,
                'options' => [
                    'class' => 'section_signup_form',
                ],
                'fieldConfig' => [
                    'template' => "<div class='section_signup_form_group'>{label}{error}{input}</div>",
                ],
            ]);
            ?>
            <?= $form->field($model, 'name')->textInput(['required' => 'required'])->label('Название организации', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'inn')->textInput()->label('Инн', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'kpp')->textInput(['required' => 'required'])->label('Кпп', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'rs')->textInput(['required' => 'required'])->label('Рассчетный счет', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'bank')->textInput(['required' => 'required'])->label('Банк', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'ks')->textInput(['required' => 'required'])->label('Кор. счет', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'bik')->textInput(['required' => 'required'])->label('БИК', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'FIO')->textInput(['required' => 'required'])->label('ФИО', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'phone')->textInput(['required' => 'required'])->label('Телефон', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'email')->textInput(['required' => 'required'])->label('email', ['class' => 'section_signup_form_title']) ?>
            <div class="section_signup_form_button">
                <?= yii\helpers\Html::submitButton('Подать заявку', ['class' => 'button button_transparent_blue', 'onclick' => "ym(53344498, 'reachGoal', 'subscribe2'); return true;"]) ?>
            </div>
            <?php \yii\widgets\ActiveForm::end(); ?>
        </div>
    </div>
</section>
<footer class="main_page_footer">
    <div class="wrapper">
        <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

            <div class="copyright">GiveCorp © 2019</div>
            <div class="main_page_footer_button"><span
                        class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
            </div>
        </div>
        <div class="footer__links">

            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                соглашение</a>
            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика обработки
                персональных данных</a>
        </div>
    </div>
</footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>
