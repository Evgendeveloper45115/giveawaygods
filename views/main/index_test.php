<?php
/* @var $blogers app\models\Blogers[] */
/* @var $top_give \app\models\Gives */

/* @var $gives \app\models\Gives[] */
?>
<?php
if (isset($_GET['popup'])) {
    $this->registerJs("
    $('#popup-thank').attr('style', 'display:flex');
");

}
?>
<div class="info-give" style="background: none">
    <div class="container">
        <h2 class="title__steps" data-aos="zoom-out" data-aos-duration="1000">
            Our Founders
        </h2>
        <ul class="info-give__items">

            <li class="info-give__item margin_top_mob" data-aos="fade-right" data-aos-duration="1000">
                <div class="info-give__content">
                    <h3 class="info-give__title--item">
                        Founder intro Alina
                    </h3>

                    <iframe src="https://www.youtube.com/embed/JVKhWeNw15U" frameborder="0"
                            allowfullscreen></iframe>
                </div>
            </li>
            <li class="info-give__item margin_top_mob" data-aos="fade-left" data-aos-duration="1000">
                <div class="info-give__content">
                    <h3 class="info-give__title--item">
                        Founder intro Julien
                    </h3>

                    <iframe src="https://www.youtube.com/embed/qYNfHoPQQFg" frameborder="0"
                            allowfullscreen></iframe>

                </div>
            </li>
        </ul>
    </div>
</div>

<div class="steps" data-aos="zoom-in" data-aos-duration="1000">
    <div class="container">
        <h2 class="title__steps">
            GIVEAWAY STEPS
        </h2>
        <ul class="step__items">
            <li class="step__item">
                <div class="step__number step__number--line">
                    1
                </div>
                <h3 class="step__name">
                    THE WARM UP
                </h3>
                <p class="step__text">
                    The Influencer gets their followers excited before the giveaway starts by informing them about the
                    upcoming giveaway and its incredible prizes.
                </p>
            </li>
            <li class="step__item">
                <div class="step__number">
                    2
                </div>
                <h3 class="step__name">
                    LAUNCH DAY
                </h3>
                <p class="step__text">
                    On the day of the giveaway launch, the Influencer encourages their followers to visit the contest
                    account.
                </p>
                <p class="step__text no_margin">
                    To win a prize, a participant needs to follow the contest account and all the sponsors that the
                    contest account is following. Our goal is both to attract the maximum amount of interest to the
                    giveaway and make it as easy as possible to participate.
                </p>
            </li>
            <li class="step__item">
                <div class="step__number">
                    3
                </div>
                <h3 class="step__name">
                    RULES OF ENGAGEMENT
                </h3>
                <p class="step__text">
                    Your new followers won’t just follow you, they will also visit your account and may like and/or
                    comment on your posts. This will improve your engagement ratings and overall activity.
                </p>
            </li>
            <li class="step__item">
                <div class="step__number step__number--last">
                    4
                </div>
                <h3 class="step__name">
                    THE FINALE
                </h3>
                <p class="step__text">
                    On a predetermined date, we do a high-profile and transparent online drawing using a random number
                    generator to pick the lucky winner. All winners are posted on our site and the contest account. As a
                    final step, we ship the prizes to the giveaway winner.
                </p>
            </li>
        </ul>
    </div>
</div>
<div class="next-give" id="next"  data-aos="zoom-in" data-aos-duration="1000">
    <h3 class="next-give__title">
        Next Giveaway
    </h3>
    <?php
    foreach ($gives as $give) {
        ?>

        <div class="container next-give__container">
            <div class="next-give__left">
                <div class="next-give__logo">
                    <img class="next-give__photo" src="img/<?= $give->bloger->avatar ?>" alt=""/>
                </div>
                <div class="next-give__desc">
                    <h3 class="next-give__name">
                        <?= $give->bloger->name ?>
                    </h3>
                    <div class="next-give__text">
                        <p class="next-give__text--2">
                            <?= $give->bloger->count_subscribes ?>
                        </p>

                        <p class="next-give__text--2">
                            <?= $give->bloger->text ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="next-give__right">
                <div class="next-give__right--container">
                    <div class="next-give__subscribe">
                        <p class="next-give__right-text next-give__right-text--indent">
                            ≈ +followers
                        </p>

                        <p class="next-give__right-text next-give__right-text--bold">
                            <?= number_format($give->subscribes_in, 0, '.', ',') ?>!
                        </p>
                    </div>
                    <div class="next-give__start">
                        <p class="next-give__right-text next-give__right-text--indent">
                            ≈start
                        </p>

                        <p class="next-give__right-text next-give__right-text--bold">
                            <?= $give->date_start ?>
                        </p>
                    </div>
                    <div class="next-give__price">
                        <p class="next-give__right-text next-give__right-text--indent">
                            Price
                        </p>

                        <p class="next-give__right-text next-give__right-text--bold">
                            $<?= number_format($give->cost, 0, '.', ',') ?>
                        </p>
                    </div>
                </div>
                <?php
                if($give->is_on){
                    ?>
                    <button onclick="fbq('track', 'InitiateCheckout')" class="next-give__right--button js-popup" data-popup="popup-participate">
                        JOIN NOW
                    </button>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="info-give">
    <div class="container">
        <h2 class="title__info" data-aos="zoom-out" data-aos-duration="1000">
            HOW WE DO IT!
            <span class="info-give__title">
                        Information for our sponsors
                    </span>
        </h2>
        <ul class="info-give__items">
            <li class="info-give__item" data-aos="fade-right" data-aos-duration="1000">
                <div class="info-give__logo">
                    <img class="info-give__png" src="img/contributors.png" alt=""/>
                </div>
                <div class="info-give__content">
                    <h3 class="info-give__title--item">
                        FOLLOWERS
                    </h3>
                    <p class="info-give__text">
                        You can gain about 5,000 to 200,000 real followers on your account per giveaway. It’s not magic,
                        it is a process-driven, simple and proven method that our team at Giveaway Gods developed and
                        successfully implemented many times.
                    </p>
                </div>
            </li>
            <li class="info-give__item" data-aos="fade-left" data-aos-duration="1000">
                <div class="info-give__logo">
                    <img class="info-give__png" src="img/assistance.png" alt=""/>
                </div>
                <div class="info-give__content">
                    <h3 class="info-give__title--item">
                        GUIDANCE
                    </h3>
                    <p class="info-give__text">
                        The average rate of unfollowing after the end of a giveaway is between 5% and 35%. The rate
                        applicable to you will depend highly on the content you post after the giveaway.
                    </p>
                    <p class="info-give__text">
                        We encourage all sponsors to create beautiful content before, during and after the giveaway.
                    </p>
                    <p class="info-give__text">
                        Before the winner is selected, we remove the list of sponsors.
                    </p>

                </div>
            </li>
            <li class="info-give__item"  data-aos="fade-right" data-aos-duration="1000">
                <div class="info-give__logo">
                    <img class="info-give__png" src="img/prizes.png" alt=""/>
                </div>
                <div class="info-give__content">
                    <h3 class="info-give__title--item">
                        PRIZES
                    </h3>
                    <p class="info-give__text">
                        Past lucky and randomly selected winners won iPhones, lots of cash, and even cars. We work
                        closely with Influencers to carefully select the right prizes for their audience which maximizes
                        engagement.
                    </p>
                </div>
            </li>
            <li class="info-give__item"  data-aos="fade-left" data-aos-duration="1000">
                <div class="info-give__logo">
                    <img class="info-give__png" src="img/safety.png" alt=""/>
                </div>
                <div class="info-give__content">
                    <h3 class="info-give__title--item">
                        SAFETY INFORMATION
                    </h3>
                    <p class="info-give__text no_margin">
                        We will never ask for your password or any private information. Also, our giveaways are in full
                        compliance with Instagram’s rules and regulations. As a contingency, we create a backup giveaway
                        account so the data and the giveaway itself are secured.
                    </p>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="next-give"   data-aos="zoom-in" data-aos-duration="1000">
    <h3 class="next-give__title">
        Next Giveaway
    </h3>
    <?php
    foreach ($gives as $give) {
        ?>
        <div class="container next-give__container">
            <div class="next-give__left">
                <div class="next-give__logo">
                    <img class="next-give__photo" src="img/<?= $give->bloger->avatar ?>" alt=""/>
                </div>
                <div class="next-give__desc">
                    <h3 class="next-give__name">
                        <?= $give->bloger->name ?>
                    </h3>
                    <div class="next-give__text">
                        <p class="next-give__text--2">
                            <?= $give->bloger->count_subscribes ?>
                        </p>

                        <p class="next-give__text--2">
                            <?= $give->bloger->text ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="next-give__right">
                <div class="next-give__right--container">
                    <div class="next-give__subscribe">
                        <p class="next-give__right-text next-give__right-text--indent">
                            ≈ +followers
                        </p>

                        <p class="next-give__right-text next-give__right-text--bold">
                            <?= number_format($give->subscribes_in, 0, '.', ',') ?>!
                        </p>
                    </div>
                    <div class="next-give__start">
                        <p class="next-give__right-text next-give__right-text--indent">
                            ≈start
                        </p>

                        <p class="next-give__right-text next-give__right-text--bold">
                            <?= $give->date_start ?>
                        </p>
                    </div>
                    <div class="next-give__price">
                        <p class="next-give__right-text next-give__right-text--indent">
                            Price
                        </p>

                        <p class="next-give__right-text next-give__right-text--bold">
                            $<?= number_format($give->cost, 0, '.', ',') ?>
                        </p>
                    </div>
                </div>
                <?php
                if($give->is_on){
                    ?>
                    <button onclick="fbq('track', 'InitiateCheckout')" class="next-give__right--button js-popup" data-popup="popup-participate">
                        JOIN NOW
                    </button>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<footer class="footer"   data-aos="zoom-in" data-aos-duration="1000">
    <div class="container">
        <p class="footer__left">
            <u>GiveawayCorp Inc. 2020 </br>All Rights Reserved</u>
        </p>
        <a href="https://www.instagram.com/giveawaygods.us" class="footer__middle" target="_blank">
            <img class="footer__inst" src="/img/inst_small.png" alt=""/>
        </a>
        <a class="footer__right footer__right-link" href="/main/terms-of-service">Terms of Service</a>
        <button class="footer__right js-popup" data-popup="popup-consultate">
            JOIN NOW!
        </button>
    </div>
</footer>
<style>
    iframe{
        width: 50rem;
        height: 29rem;
    }
</style>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>



