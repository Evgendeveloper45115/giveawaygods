<section class="section_agency">
    <div class="section_agency_content"></div>
    <header class="header_transparent">
        <div class="wrapper">
            <div class="header"><a class="header_logo logo" href="/">givecorp</a>
                <div class="header_right"><a class="header_login login_opener" href="#">Войти</a><a
                            class="header_menu menu_opener" href="#"><span></span><span
                                class="header_menu_middle"></span><span></span></a></div>
            </div>
        </div>
    </header>
    <div class="wrapper">
        <h1>Рекламное агентство Givecorp</h1>
        <div class="agency_content_afterh1">создание мощных брендов и продвижение в инстаграм</div>
        <a class="button button_transparent_blue getconsultation_opener" href="#">Получить консультацию</a>
        <div class="agency_content_logo"></div>
    </div>
</section>
<section class="section_about">
    <div class="wrapper">
        <div class="about_text">Первое, что видит человек в вашем аккаунте, — информацию о вас. Поэтому в описании
            профиля кратко нужно сказать, о чем он, зачем на вас нужно подписаться. Если это бренд — что он продает.
            Если личный блог — о чем он.
        </div>
    </div>
</section>
<section class="section_howwedo">
    <div class="wrapper">
        <div class="howwedo_title">Как мы делаем это</div>
        <div class="howwedo_aftertitle">успех «под ключ»</div>
        <div class="howwedo_list">
            <div class="howwedo_item howwedo_item_reverse">
                <div class="howwedo_item_img"><img src="/img/howwedo_img1.png"></div>
                <div class="howwedo_item_text">
                    <h2>Название и логотип</h2>
                    <div class="howwedo_item_p">Мы берем на себя работу по созданию бренда под ключ, начиная с логотипа
                        и названия! Проведя анализ планируемого ассортимента и направления бренда, мы подготовим
                        варианты на согласование с заказчиком.
                    </div>
                    <div class="howwedo_item_text_bottom">
                        <div class="howwedo_item_text_cost">от 200 000.-</div>
                        <a class="link getconsultation_opener_custom" href="#">Заказать</a>
                    </div>
                </div>
            </div>
            <div class="howwedo_item">
                <div class="howwedo_item_img"><img src="/img/howwedo_img2.png"></div>
                <div class="howwedo_item_text">
                    <h2>Упаковка</h2>
                    <div class="howwedo_item_p">
                        Следующим шагом формирования фирменного стиля будет упаковка будущей продукции. Основываясь на
                        нашем огромном опыте, мы подберем максимально подходящую тару и придадим ей привлекательный
                        стильный вид. Именно с упаковки ваш клиент начинает свое знакомство с продуктом, будь то фото в
                        инстаграме или уже на прилавках торговых центров!
                    </div>
                    <div class="howwedo_item_text_bottom">
                        <div class="howwedo_item_text_cost">от 500 000.-</div>
                        <a class="link getconsultation_opener_custom" href="#">Заказать</a>
                    </div>
                </div>
            </div>
            <div class="howwedo_item howwedo_item_reverse">
                <div class="howwedo_item_img"><img src="/img/howwedo_img3.png"></div>
                <div class="howwedo_item_text">
                    <h2>Сайт</h2>
                    <div class="howwedo_item_p">Услышав о вашем бренде, человек, первым делом, захочет найти информацию
                        о нем в интернете. Сайт завершит формирование образа вашего бренда. Мы разработаем сайт любой
                        сложности для вашего продукта, будь то просто информативный лендинг, или полноценный интернет
                        магазин с огромной товарной матрицей, интеграцией c СRМ, 1С, Касс и инструментов коммуникации с
                        клиентом.

                    </div>
                    <div class="howwedo_item_text_bottom">
                        <div class="howwedo_item_text_cost">от 600 000.-</div>
                        <a class="link getconsultation_opener_custom" href="#">Заказать</a>
                    </div>
                </div>
            </div>
            <div class="howwedo_item">
                <div class="howwedo_item_img"><img src="/img/howwedo_img4.png"></div>
                <div class="howwedo_item_text">
                    <h2>Instagram-контент</h2>
                    <div class="howwedo_item_p">Чтобы инстаграм подогревал вашу аудиторию и являлся полноценным
                        инструментом продажи – необходимы качественные профессиональные фото! Мы возьмем все эти заботы
                        на себя: модели, студии, съемки, ретуширование и составление контент плана на месяц вперед,
                        предоставив его вам на согласование.
                    </div>
                    <div class="howwedo_item_text_bottom">
                        <div class="howwedo_item_text_cost">от 60 000.-</div>
                        <a class="link getconsultation_opener_custom" href="#">Заказать</a>
                    </div>
                </div>
            </div>
            <div class="howwedo_item howwedo_item_reverse">
                <div class="howwedo_item_img"><img src="/img/howwedo_img5.png"></div>
                <div class="howwedo_item_text">
                    <h2>Продвижение и PR-стратегия </h2>
                    <div class="howwedo_item_p">
                        Качественно оформленный и привлекательный бренд все равно требует продвижения. Мы подготовим
                        план продвижения, и, в течение всей работы, вы будете получать планы развития на согласование с
                        вами. О вашем бренде будут говорить топовые блогеры и его название будет светиться в самых
                        громких ивентах!
                    </div>
                    <div class="howwedo_item_text_bottom">
                        <div class="howwedo_item_text_cost">от 100 000.-</div>
                        <a class="link getconsultation_opener_custom" href="#">Заказать</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="promotion_list">
            <div class="promotion_item">
                <div class="promotion_item_icon" style="background-image: url(/img/promotion_icon1.png)"></div>
                <div class="promotion_item_text">Разработка коммуникационной стратегии</div>
            </div>
            <div class="promotion_item">
                <div class="promotion_item_icon" style="background-image: url(/img/promotion_icon2.png)"></div>
                <div class="promotion_item_text">Подбор подходящих лидов, мнений</div>
            </div>
            <div class="promotion_item">
                <div class="promotion_item_icon" style="background-image: url(/img/promotion_icon3.png)"></div>
                <div class="promotion_item_text">Размещение рекламы и аналитика</div>
            </div>
        </div>
        <div class="howwedo_list">
            <div class="howwedo_item">
                <div class="howwedo_item_img"><img src="/img/howwedo_img6.png"></div>
                <div class="howwedo_item_text">
                    <h2>Рекламная кампания</h2>
                    <div class="howwedo_item_p">Мы подготовим рекламную кампанию, предложив несколько вариантов бюджета.
                        Вы будете в курсе о каждом потраченном рубле на рекламную кампанию и легко сможете оценить
                        эффективность нашей работы по объему продаж вашего бренда!
                    </div>
                    <div class="howwedo_item_text_bottom">
                        <div class="howwedo_item_text_cost">от 100 000.-</div>
                        <a class="link getconsultation_opener_custom" href="#">Заказать</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="advertising_list">
            <div class="promotion_item">
                <div class="promotion_item_icon" style="background-image: url(/img/promotion_icon4.png)"></div>
                <div class="promotion_item_text">Таргетинг</div>
            </div>
            <div class="promotion_item">
                <div class="promotion_item_icon" style="background-image: url(/img/promotion_icon5.png)"></div>
                <div class="promotion_item_text">Ретаргетинг</div>
            </div>
            <div class="promotion_item">
                <div class="promotion_item_icon" style="background-image: url(/img/promotion_icon6.png)"></div>
                <div class="promotion_item_text">Контекстная реклама</div>
            </div>
        </div>
        <div class="howwedo_bottomtext">
            <h2 class="fz28">Когда каждый этап выполнен <br>на высшем уровне - потрясающий  <br>результат неизбежен!
            </h2><a
                    class="link getconsultation_opener_custom" href="#">Начать сотрудничество</a>
        </div>
        <div class="bigemeilbuttoon"><a href="mailto:agency@givecorp.ru">agency@givecorp.ru</a></div>
    </div>
</section>
<footer class="main_page_footer">
    <div class="wrapper">
            <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>
                
                <div class="copyright">GiveCorp © 2019</div>
                <div class="main_page_footer_button"><span
                            class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
                </div>
            </div>
            <div class="footer__links">
            
                <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                        соглашение</a>
                    <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика обработки
                    персональных данных</a>
            </div>
        </div>
</footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>
