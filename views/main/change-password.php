<?php

use yii\widgets\ActiveForm;

/**@var $model \app\models\User */
if($success){
    $this->registerJs("
        $('.sent_popup_custom').fadeIn()
        $('body').addClass('fixed');
    ");
}
?>

    <section class="section_signup" >
        <div class="section_signup_inner">
            <div class="section_signup_header"><a class="logo logo_black"
                                                  href="<?= \yii\helpers\Url::to(['/main/gives']) ?>">GIVECORP</a><a
                        class="header_menu menu_opener" href="#"><span></span><span
                            class="header_menu_middle"></span><span></span></a></div>
            <div class="section_signup_content_custom">
                <h1>Смена пароля клиента: <?= $model->first_name . ' ' . $model->last_name ?> </h1>
                <?php $form = ActiveForm::begin([
                    'id' => 'form-signsup',
                    'validateOnChange' => true,
                    'validateOnSubmit' => true,
                    'enableClientValidation' => true,
                    'options' => [
                        'class' => 'section_signup_form',
                    ],
                    'fieldConfig' => [
                        'template' => "<div class='section_signup_form_group section_signup_form_focused'>{label}{input}{error}</div>",
                    ],
                ]);
                ?>
                <?= $form->field($model, 'orig_password')->passwordInput()->label('Старый пароль', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'new_password')->passwordInput()->label('Новый пароль', ['class' => 'section_signup_form_title']) ?>
                <?= $form->field($model, 'password1')->passwordInput()->label('Повторить новый пароль', ['class' => 'section_signup_form_title']) ?>
                <?= yii\helpers\Html::submitButton('Сменить пароль', ['class' => 'button button_transparent_blue']) ?>

                <?php ActiveForm::end(); ?>
            </div>
       </div>
    </section>
    <footer class="main_page_footer">
        <div class="wrapper">
            <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

                <div class="copyright">GiveCorp © 2019</div>
                <div class="main_page_footer_button"><span class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
                </div>
            </div>
            <div class="footer__links">

                <a target="_blank" href="/main/agreement" class="footer__link">Пользовательское
                    соглашение</a>
                <a target="_blank" href="/main/politic" class="footer__link">Политика обработки
                    персональных данных</a>
            </div>
        </div>
    </footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>