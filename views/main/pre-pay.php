<?php
/**
 * Created by PhpStorm.
 * User: evgen
 * Date: 15.04.2019
 * Time: 12:21
 */
?>
<section class="section_signup_custom">
    <div class="section_signup_inner">
        <div class="pay-list">
            <div class="pay_title" style="margin-top: 40px;">Участие в гиве временно недоступно</div>
            <div class="pay_title" style="margin-top: 40px;">Попробуйте оплатить участие позже</div>
            <div class="pay_list">
                <label class="pay_item">
                    <a href="<?= Yii::$app->request->referrer ?>">
                        <div class="pay__item pay__item_custom" style="width: 105%">
                            <div class="pay_item_name">Вернутся</div>
                        </div>
                    </a>
                </label>
            </div>
        </div>
    </div>
</section>
<footer class="main_page_footer">
    <div class="wrapper">
        <div class="main_page_footer_inner"><a class="logo logo_grey" href="/">givecorp</a>

            <div class="copyright">GiveCorp © 2019</div>
            <div class="main_page_footer_button"><span
                        class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
            </div>
        </div>
        <div class="footer__links">

            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/agreement']) ?>" class="footer__link">Пользовательское
                соглашение</a>
            <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/politic']) ?>" class="footer__link">Политика
                обработки
                персональных данных</a>
        </div>
    </div>
</footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>
