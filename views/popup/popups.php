<div class="popup" id="popup-consultate" style="z-index: 101">
    <div class="popup__container">
        <button class="popup__exit--button js-close-popup" data-popup="popup-consultate">

        </button>
        <div class="popup__title">

            <h2 class="popup__name">
                get a free consultation
            </h2>

        </div>
        <form action="<?= \yii\helpers\Url::to(['/main/consultation']) ?>" method="post" class="form  form--2">
            <div class="form__name--number">
                <div class="form__name">
                    <input type="text" name="name" id="name" placeholder="name" required="required">
                </div>
                <div class="form__number">
                    <input type="tel" name="number" id="number" placeholder="phone number" required="required">
                </div>
            </div>
            <div class="form__submit">
                <button onclick="fbq('track', 'Lead')" type="submit" class="form__btn">submit</button>
            </div>
        </form>

    </div>
</div>
<div class="popup" id="popup-participate" style="z-index: 101">
    <div class="popup__container">
        <button class="popup__exit--button js-close-popup" data-popup="popup-participate">

        </button>
        <div class="popup__title">

            <h2 class="popup__name">
                Become a sponsor</br> in Apryl Jones' Giveaway
            </h2>
            <p class="popup__text">
                By clicking the "Submit" button, you consent<br> to the processing of <a target="_blank"
                                                                                         style="text-decoration: none;border-bottom: 1px solid;"
                                                                                         href="<?= \yii\helpers\Url::to(['/main/privacy-policy']) ?>">personal
                    data</a>.
            </p>
        </div>
        <form class="form" action="<?= \yii\helpers\Url::to(['/main/consultation']) ?>" method="post">
            <div class="reg__name">
                <input type="text" name="name" id="name" placeholder="name" required="required">
            </div>
            <div class="reg__name">
                <input type="email" name="email" id="name" placeholder="email" required="required">
            </div>
            <div class="reg__inst">
                <input type="text" name="inst" id="inst" placeholder="Instagram account" required="required">
            </div>
            <div class="reg__number">
                <input type="tel" name="number" id="number" placeholder="phone number" required="required">
            </div>
            <div class="popup__link--submit">
                <div class="reg__link">
                    <label for="agree">
                        <input type="checkbox" name="agree" id="agree" checked>
                        <svg id="checkbox-checked" viewBox="0 0 28 28" fill="#fff" class="checkbox__svg">
                            <path d="M0 0v28h28V0H0zm24 24H4V4h20v20zm-2-13l-2.828-2.828-6.768 6.982-3.576-3.576L6 14.406l6.404 6.406L22 11z"></path>
                            <path d="M0 0v28h28V0H0zm24 24H4V4h20v20z"/>
                        </svg>
                    </label>

                    <a target="_blank" href="<?= \yii\helpers\Url::to(['/main/personal-data']) ?>" class="popup__link">
                        <u>I agree with the Terms and Conditions</u>
                    </a>
                </div>
                <div class="reg__submit">
                    <button onsubmit="gtag('event', 'submit', {'event_category': 'form'});" type="submit" class="popup__submit">submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="popup" id="popup-thank" style="z-index: 101">
    <div class="popup__container popup__container--thank">
        <button class="popup__exit--button js-close-popup" data-popup="popup-thank">

        </button>
        <div class="popup__title">

            <h2 class="popup__name popup__name--thank">
                Thank you!
            </h2>
            <p class="popup__text popup__text--thank">
                The request for consultation has been sent!
            </p>
            <p class="popup__text popup__text--thank">
                The Manager will contact you as soon as possible.
            </p>
        </div>
        <div class="form__submit">
            <button type="submit" class="form__btn js-close-popup" data-popup="popup-thank">Continue</button>
        </div>
    </div>
</div>