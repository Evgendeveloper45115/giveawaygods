<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Promotion */

$this->title = 'Реклама ИД ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Promotion'), 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="gives-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'bloger_id',
                'value' => function (\app\models\Promotion $model) {
                    return $model->bloger->instagram_name;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function (\app\models\Promotion $model) {
                    return $model->statuses()[$model->status];
                }
            ],

            'auditory',
            'interest',
            'count_seat',
            'plus_subscriber',
            'all_cost',
        ],
    ]) ?>

    <?php
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $model->getPromotionHasUsers(),
        'pagination' => [
            'pageSize' => 20,
        ],
    ]);

    ?>
    <div><h2>Участвуют в данной Рекламе</h2></div>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'user.first_name',
            'user.last_name',
            'user.email',
            'user.instagram_ak',
            'user.phone',

            //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
