<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gives */
/* @var $form yii\widgets\ActiveForm */
/* @var $blogers \app\models\Blogers[] */

?>

<div class="gives-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bloger_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $blogers,
        'language' => 'ru',
        'options' => ['placeholder' => 'Выбрать блогера'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'all_cost')->textInput() ?>
    <span>Аудитория</span>
    <?= $form->field($model, 'auditory', [
        'template' => "<div>{input}\n<div class=\"error\">{error}</div></div>",
    ])->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'options' => ['rows' => 15],
        'language' => 'ru',
//        'spellcheckerUrl'=>'http://speller.yandex.net/services/tinyspell',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]); ?>
    <?php
    if (!$model->isNewRecord) {
        echo $form->field($model, 'sort')->textInput();
    }
    ?>
    <?= $form->field($model, 'interest')->textInput() ?>
    <?= $form->field($model, 'count_seat')->textInput() ?>
    <?= $form->field($model, 'plus_subscriber')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
