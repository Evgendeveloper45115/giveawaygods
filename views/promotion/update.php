<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gives */
/* @var $blogers \app\models\Blogers[] */

$this->title = Yii::t('app', 'Редактирование рекламы  {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Реклама'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="gives-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'blogers' => $blogers
        ,
    ]) ?>

</div>
