<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GivesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Реклама');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gives-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать рекламу'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'bloger_id',
                'value' => function (\app\models\Promotion $model) {
                    return $model->bloger->name;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function (\app\models\Promotion $model) {
                    return $model->statuses()[$model->status];
                }
            ],
            'auditory',
            'interest',
            'count_seat',
            'plus_subscriber',
            'all_cost',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {view} {new} {finish}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', \yii\helpers\Url::to(['/promotion/update', 'id' => $model->id]), [
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', \yii\helpers\Url::to(['/promotion/delete', 'id' => $model->id]), [
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', \yii\helpers\Url::to(['/promotion/view', 'id' => $model->id]), [
                        ]);
                    },
                    'new' => function ($url, $model) {
                        return Html::a('<i title="Новый" class="glyphicon glyphicon-ok"></i>', \yii\helpers\Url::to(['/promotion/new', 'id' => $model->id]), [
                        ]);
                    },
                    'finish' => function ($url, $model) {
                        return Html::a('<i title="Закончить" class="glyphicon glyphicon-remove"></i>', \yii\helpers\Url::to(['/promotion/finish', 'id' => $model->id]), [
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
