<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BlogersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Блогеры');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a(Yii::t('app', 'Создать Блогера'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'avatar',
                'format' => 'raw',
                'value' => function (\app\models\Blogers $bloger) {
                    return Html::img('/img/' . $bloger->avatar, ['style' => 'width:200px']);
                }
            ],
            [
                'attribute' => 'statistics_img',
                'format' => 'raw',
                'value' => function (\app\models\Blogers $bloger) {
                    return Html::img('/img/' . $bloger->statistics_img, ['style' => 'width:200px']);
                }
            ],
            [
                'attribute' => 'statistics_img_two',
                'format' => 'raw',
                'value' => function (\app\models\Blogers $bloger) {
                    return Html::img('/img/' . $bloger->statistics_img_two, ['style' => 'width:200px']);
                }
            ],
            'instagram',
            'name',
            'count_subscribes',
            'text:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
