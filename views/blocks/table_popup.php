<?php
/**
 * @var $give \app\models\Gives
 */
?>
<div class="table_popup table_popup_top table_popup_bloger"><span
            class="table_popup_close"><svg
                viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" fill="rgb(77, 115, 215)"
                                                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.518,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.480,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
                                        </svg></span>
    <div class="table_popup_bloger_photo"
         style="background-image: url(/img/<?= $give->bloger->avatar ?>)"></div>
    <div class="table_popup_bloger_name"><?= $give->bloger->name ?></div>
    <?php
    $arr = null;
    $arr2 = null;
    if (stristr($give->bloger->instagram, ',')) {
        $arr = explode(',', $give->bloger->instagram);
        $arr2 = explode(',', $give->bloger->instagram_name);
    }
    if ($arr != null) {
        ?>
        <div style="display: inline-flex">
            <a class="table_popup_bloger_inst" href="<?= $arr[0] ?>"><?= $arr2[0] ?></a>
            <span class="plus">&nbsp;+&nbsp;</span>
            <a class="table_popup_bloger_inst" href="<?= $arr[1] ?>"><?= $arr2[1] ?></a>
        </div>
        <?php
        if (isset($arr[2])) {
            ?>
            <div style="display: inline-flex">
                <a class="table_popup_bloger_inst" href="<?= $arr[2] ?>"><?= $arr2[2] ?></a>
                <span class="plus">&nbsp;+&nbsp;</span>
                <a class="table_popup_bloger_inst" href="<?= $arr[3] ?>"><?= $arr2[3] ?></a>
            </div>
            <?php
        }
    } else {
        ?>
        <a class="table_popup_bloger_inst"
           href="<?= $give->bloger->instagram ?>"><?= $give->bloger->instagram_name ?></a>
        <?php
    }
    ?>
    <div class="table_popup_bloger_sub"><?= $give->bloger->count_subscribes ?>
        подписчиков
    </div>
    <div class="table_popup_bloger_text"><?= $give->bloger->text ?>
    </div>
    <!-- <a class="table_popup_bloger_link"
       href="<? /*= \yii\helpers\Url::to(Yii::$app->user->isGuest ? '/main/registration' : ['/main/gives-completed', 'bloger' => $give->bloger_id]) */ ?>">Проведенные
        гивы</a>-->
    <div class="table_popup_bloger_bottom">
        <div class="table_popup_bloger_bottom_next">Следующий
            гив: с <?= \app\models\User::russian_date(date('d.m.Y', strtotime($give->date_start))) ?><br>
            по <?= \app\models\User::russian_date(date('d.m.Y', strtotime($give->date_end))) ?></div>
        <div class="table_popup_bloger_bottom_cost">
                                        <span>Мест: <?= trim(($give->plus_subscriber ? count($give->giveHasUsers) + $give->plus_subscriber : count($give->giveHasUsers))) ?>
                                            /<?= $give->count_seats ?></span>
            <?php
            if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != 'index-test') {
                ?>
                <a onclick="ym(53344498, 'reachGoal', 'subscribe'); return true;"
                   style="margin-right: 0" class="section_givs_cost_getprice"
                   href="<?= \yii\helpers\Url::to('main/registration') ?>"><?= 'Узнать цену' ?></a>
                <?php
            } else {
                ?>
                <span>Стоимость: <?= $give->all_cost ?>руб.</span>
                <?php
            }
            ?>
        </div>
        <?php
        if (Yii::$app->controller->action->id == 'index-test') {
            ?>
            <a onclick="ym(53344498, 'reachGoal', <?= Yii::$app->controller->action->id == 'index' ? 'subscribe' : 'join' ?>); return true;"
               class="link"
               href="<?= \yii\helpers\Url::to(['/main/pre-pay', 'cost' => $give->all_cost, 'give_id' => $give->id]) ?>">Участвовать</a>
            <?php
        } else {
            ?>
            <a onclick="ym(53344498, 'reachGoal', <?= Yii::$app->controller->action->id == 'index' ? 'subscribe' : 'join' ?>); return true;"
               class="link"
               href="<?= $url_custom ? $url_custom : (Yii::$app->user->isGuest ? '/main/registration' : '/main/gives') ?>"><?= $text_custom ? $text_custom : 'Участвовать' ?></a>
            <?php
        }
        ?>
    </div>
</div>
