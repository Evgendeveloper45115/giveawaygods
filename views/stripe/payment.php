<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167190189-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-167190189-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5VQKSNG');</script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5VQKSNG');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '605878313672331');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=605878313672331&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <meta charset="utf-8"/>
    <title>Stripe Card Elements sample</title>
    <meta name="description" content="Stripe Payment Intents"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/css/normalize.css?v=0.2"/>
    <link rel="stylesheet" href="/css/global.css?v=0.2"/>
    <link rel="stylesheet" href="/css/style.css?v=0.2"/>
    <link rel="stylesheet" href="/css/stripe.css?v=0.1"/>
    <script src="/js/jquery.js?v=0.1"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="/js/script.js?v=0.2" defer></script>
</head>

<body>
<script>
    fbq('track', 'AddPaymentInfo');
</script>

<h2 class="popup__name payment-title">
    Payment for participation<br>as a Sponsor<br>in Apryl Jones' June Giveaway $990
</h2>
<div class="text-payment__block">
    <p class="text-payment text-payment__top">
        The Manager will contact you after payment!
    </p>
    <p class="text-payment text-payment__bottom">
        Sponsorship places occupied: <b>37 out of 50</b>
    </p>
</div>

<div class="sr-root">
    <div class="sr-main">
        <form id="payment-form" class="sr-payment-form">
            <div class="sr-combo-inputs-row">
                <div class="sr-input sr-card-element" id="card-element"></div>
            </div>
            <div class="sr-field-error" id="card-errors" role="alert"></div>
            <button id="submit">
                <div class="spinner hidden" id="spinner"></div>
                <span id="button-text">Pay</span><span id="order-amount"></span>
            </button>
        </form>
        <div class="sr-result hidden">
            <p class="after">Payment completed<br/></p>
            <div class="after_div">
                <code></code>
            </div>
        </div>
    </div>
</div>
</body>
</html>
