<?php
namespace app\services\payment;

class PaymentData
{
    public $userId;
    public $invoiceId;
    public $amount;
    public $give_id;
}