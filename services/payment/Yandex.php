<?php

namespace app\services\payment;

use Yii;
use yii\helpers\VarDumper;
use yii\web\Request;

class Yandex implements IPaymentGatewayWithPreCheck
{
    private $url;
    private $shopId;
    private $scid;
    private $paymentType;

    public function __construct()
    {
        $this->url = Yii::$app->params['url'];
        $this->shopId = Yii::$app->params['shopId'];
        $this->scid = Yii::$app->params['scid'];

    }

    public function renderForm($user, $cost, $give_id)
    {
        $this->paymentType = Yii::$app->request->post('payment_type');
        return Yii::$app->controller->renderPartial('../_form/auto-submit-form', [
            'user' => $user,
            'cost' => $cost,
            'give_id' => $give_id,
            'paymentType' => $this->paymentType,
            'url' => $this->url,
            'shopId' => $this->shopId,
            'scid' => $this->scid,
        ]);
    }

    public function isValidPayment(Request $request)
    {
        return $this->checkMD5($request);
    }

    public function renderSuccessCheckView()
    {
        return $this->renderView('check_success', 0);
    }

    public function renderFailedCheckView()
    {
        return $this->renderView('check_success', 200);
    }

    public function renderSuccessView()
    {
        return $this->renderView('aviso_success', 0);
    }

    public function renderFailedView()
    {
        return $this->renderView('aviso_success', 200);
    }

    public function getPaymentData(Request $request)
    {

        $data = new PaymentData();
        $data->userId = $request->post('customerNumber');
        $data->invoiceId = $request->post('invoiceId');
        $data->amount = $request->post('orderSumAmount');
        $data->give_id = $request->post('custom_field');

        return $data;
    }

    private function renderView($view, $code)
    {
        return Yii::$app->controller->renderPartial($view, [
            'date' => Yii::$app->request->post('requestDatetime'),
            'code' => $code,
            'invoiceId' => Yii::$app->request->post('invoiceId'),
            'shopId' => Yii::$app->params['shopId'],
        ]);
    }

    private function checkMD5(Request $request)
    {

        $str = $request->post('action') . ";"
            . $request->post('orderSumAmount') . ";"
            . $request->post('orderSumCurrencyPaycash') . ";"
            . $request->post('orderSumBankPaycash') . ";"
            . $this->shopId . ";"
            . $request->post('invoiceId') . ";"
            . $request->post('customerNumber') . ";"
            . Yii::$app->params['shopPassword'];


        $md5 = strtoupper(md5($str));

        if ($md5 != strtoupper($request->post('md5'))) {
            return false;
        }
        return true;
    }
}