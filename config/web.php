<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'main/index',
    'components' => [
        'amocrm' => [
            'class' => 'yii\amocrm\Client',
            //  'subdomain' => 'example', // Персональный поддомен на сайте amoCRM
            'login' => 'info@givecorp.ru', // Логин на сайте amoCRM
            'hash' => '8d41ce120d539b58b48a874f2eb016272a069652', // Хеш на сайте amoCRM

        ],
        'stripe' => [
            'class' => 'ruskid\stripe\Stripe',
            'publicKey' => "pk_test_PZAzqjYZLJ2Z58Ux4ESqsX8a001rdvqcla",
            'privateKey' => "sk_test_c9ve1XzN3mGEYtcyDdtdsjSx00Qam1OHf3",
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    'js' => [
                    ]
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'FSXi9JkgsmLEX90kadILdhFrikWVSvMU',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'zyx\phpmailer\Mailer',
            'viewPath' => '@app/mail',
            'htmlView' => '@app/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'from' => 'sysgiveawaycorp@gmail.com'
            ],
            'config' => [
                'mailer' => 'smtp',
                'host' => 'smtp.gmail.com',
                'port' => '587',
                'smtpsecure' => 'tls',
                'smtpauth' => true,
                'username' => 'sysgiveawaycorp@gmail.com',
                'password' => 'Qq123456!',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget', //в файл
                    'categories' => ['payment_fail'], //категория логов
                    'logFile' => '@runtime/logs/pay.log', //куда сохранять
                    'logVars' => [] //не добавлять в лог глобальные переменные ($_SERVER, $_SESSION...)
                ],
            ],
        ], 'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'ad' => 'main/promotion'
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
