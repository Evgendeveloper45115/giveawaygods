<?php

use yii\helpers\Html;

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
        <style type="text/css">


            * {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 15px;
            }

            div, p, span, strong, b, em, i, a, li, td {
                -webkit-text-size-adjust: none;
            }

            img {
                max-width: 100%;
            }

            @media only screen and (max-device-width: 748px), only screen and (max-width: 748px) {
                body {
                    margin: 0 auto;
                    max-width: 320px;
                }

                table {
                    width: 320px !important;
                    margin: 0 auto !important;
                }

                span.size28 {
                    font-size: 28px !important;
                }

                .block {
                    display: block;
                    width: 98% !important;
                    margin: 0 auto !important;
                }

                .margin0 {
                    margin: 0 !important;
                }

                .margin_bl {
                    display: block;
                    margin: 0 auto 28px;
                }

                .button {
                    width: 295px !important;
                    margin-top: 30px !important;
                }

                .padding {
                    padding: 6px 0 0 32px !important;
                }

                .block2 {
                    display: table;
                    width: 145px !important;
                    vertical-align: bottom;
                }

                .block3 {
                    width: 140px !important;
                    display: table;
                }

                .block4 {
                    width: 140px !important;
                    display: table;
                }

                .height0 {
                    height: 0 !important;
                }

                .center {
                    font-size: 14px !important;
                }

                .width155 {
                    width: 147px !important;
                }

                .widht150 {
                    width: 150px !important;
                }

                .height {
                    height: 0 !important;
                }

                .float {
                    float: none !important;
                    margin: 0 auto !important;
                }

                .none {
                    display: none !important;
                }

                .width100 {
                    width: 100% !important;
                }

                .paddi {
                    padding: 6px 0 0 0 !important;
                }

                .padding77 {
                    padding: 0 0 0 77px !important;
                }

                .padding75 {
                    padding: 7px 0 0 75px !important;
                }
        </style>

        <?php $this->head() ?>
    </head>
    <body style="margin: 0 auto;padding: 0;max-width: 750px; width: 100%;">
    <?php $this->beginBody() ?>
    <table cellpadding="0" cellspacing="0" width="100%" border="0"
           style="min-width:300px;max-width: 740px; border-collapse:collapse;" align="center">
        <tr>
            <td align="center">

                <table cellpadding="0" cellspacing="0" width="100%" border="0"
                       style="min-width:300px;max-width: 740px; border-collapse:collapse;background:#000000 url(https://givecorp.ru/img/title_email.jpg) no-repeat top center;background-size: cover; "
                       align="center" bgcolor="#55bd9d">
                    <tr>
                        <td align="center">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0"
                                   style="border-collapse:collapse;max-width: 650px;" align="center">
                                <tr>
                                    <td height="280" align="center" valign="top">
                                        <table align="center" style="max-width: 650px; margin-top: 30px;">
                                            <tr>
                                                <td align="center" width="96">
                                                    <a href="#" target="_blank">
                                                        <img src="https://givecorp.ru/img/logo_mail.png"
                                                             alt=""/>
                                                    </a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <span
                                                            style="display: block; font-family: 'Roboto Slab', serif; font-size: 34px;color: #ffffff;text-align: center;line-height: 35px;padding-top: 15px;"
                                                            class="size28">Вам открыт доступ для участия в гивах!</span>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <!-- End header -->


                <table width="100%" align="center" border="0" style="height: 100px;max-width: 505px;">
                    <tr>
                        <td width="1" height="20"></td>
                    </tr>
                    <tr>
                        <td align="center" valign="center">
                            <span style="text-align: center;color: #676767;font-size: 17px;">Чтобы попасть в личный кабинет перейдите по ссылке:</span>
                            <a href="https://givecorp.ru/main/gives?email=<?= $user->email ?>" target="_blank"
                               style="text-transform:uppercase; width: 340px;height: 40px;padding:20px 0 0 0;color: #ffffff; margin: 15 auto;background:#4d73d7;border-radius: 50px;border:1px solid #4d73d7;display: block;text-decoration: none;display: block;font-family: 'Roboto Slab', serif;font-weight:600;">Войти
                                в личный кабинет</span></a>
                        </td>
                    </tr>
                </table>


                <table cellpadding="0" cellspacing="0" width="100%" border="0"
                       style="min-width:320px; border-collapse:collapse;" align="center">
                    <tbody>
                    <tr>
                        <td width="1" height="10"></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center">
                                <tbody>
                                <tr>
                                    <td align="center" valign="center">
                                        <span style="display: block; color: #676767;font-weight:600;display: block;">Ваш логин для входа: <span
                                                    style="color:#4d73d7;font-size: 17px;font-weight:600px;text-decoration:none;"><?= $user->email ?></span></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td width="1" height="5"></td>
                    </tr>

                    <tr>
                        <td align="center" valign="center">
                            <span style="color: #676767;font-weight:600;">Ваш пароль:</span> <span
                                    style="color:#4d73d7;font-size: 17px;font-weight:600px;text-decoration:none;"><?= $user->password ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td width="1" height="10"></td>
                    </tr>
                    </tbody>
                </table>


                <table width="100%" align="center" border="0"
                       style="border-collapse:collapse; max-width: 680px;min-width: 300px;">
                    <tr>
                        <td width="1" height="30"></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="left" width="100%" border="0"
                                   style="border-collapse:collapse;max-width: 440px;">
                                <tr>
                                    <td width="1" height="1"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="100%"
                                        style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;"
                                        class="widht300 margin0 center">Привет, на связи Артем Чекалин <a
                                                href="https://www.instagram.com/artem_chek/" target="_blank"><span
                                                    style="color: #4d73d7;font-weight: 600;"
                                                    class="center">@artem_chek</span></a>, создатель и идейный
                                        вдохновитель проекта <b>GiveCorp</b>! Напоминаю о том, что вам предоставлена
                                        возможность принять участие в качестве спонсора в одном из ближайших именных
                                        гивов. Чтобы стать спонсором нужно перейти <a
                                                href="https://givecorp.ru/main/gives?email=<?= $user->email ?>" target="_blank"><span
                                                    style="color: #4d73d7;font-weight: 600;"
                                                    class="center">по ссылке</span></a>, войти в Личный кабинет, если вы
                                        еще не залогинены, и нажать на кнопку "<b>Участвовать</b>" рядом с интересующим
                                        вас именным гивом. После оплаты вы будете оповещенны о точной дате старта гива!
                                    </td>
                                </tr>

                                <tr>
                                    <td width="1" height="10"></td>
                                </tr>

                            </table>


                            <table align="left" width="100%" border="0"
                                   style="border-collapse:collapse;max-width: 440px;">
                                <tr>
                                    <td width="1" height="1"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="100%"
                                        style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;"
                                        class="widht300 margin0 center">Став спонсором, вы гарантированно получите
                                        обещанное количество подписчиков, эта гарантия отражена в договоре-оферте!
                                    </td>
                                </tr>

                                <tr>
                                    <td width="1" height="10"></td>
                                </tr>

                            </table>

                            <table align="left" width="100%" border="0"
                                   style="border-collapse:collapse;max-width: 440px;">
                                <tr>
                                    <td width="1" height="1"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="100%"
                                        style="color: #676767;line-height: 25px;font-weight: 300;font-size: 17px;"
                                        class="widht300 margin0 center">Если вам нужна консультация по теме, пожалуйста,
                                        напишите нам в whatsapp по ссылке ниже, мы с радостью ответим на все вопросы!
                                    </td>
                                </tr>

                                <tr>
                                    <td width="1" height="20"></td>
                                </tr>

                            </table>

                            <table width="100%" border="0" style="border-collapse:collapse;max-width: 182px;">
                                <tr>
                                    <td width="182" class="widht300 block" style="display: block;margin:10px 0 0 47px;">
                                        <img src="https://givecorp.ru/img/star_artem.png" alt="" width="182"/>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%" align="center" border="0"
                                   style="border-collapse:collapse;max-width: 740px;min-width: 300px;"
                                   bgcolor="#f9f9f9">
                                <tr>
                                    <td>
                                        <table width="100%" align="center" border="0"
                                               style="border-collapse:collapse;max-width: 600px;min-width: 300px;">
                                            <tr>
                                                <td width="1" height="20"></td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <p style="text-align: center;color: #676767;line-height: 20px;">
                                                        Связаться с нами:</p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="1" height="20"></td>
                                            </tr>

                                            <tr>
                                                <td align="center">
                                                    <a href="https://api.whatsapp.com/send?phone=79104224246&text=&source=&data="
                                                       target="_blank"
                                                       style="text-transform:uppercase; width: 340px;height: 44px;padding:22px 0 0 0;color: #ffffff; margin: 0 auto;background:#1fe2a6;border-radius: 50px;border:1px solid #1fe2a6;display: block;text-decoration: none;display: block;font-family: 'Roboto Slab', serif;font-weight:600;">Написать
                                                        на whatsapp</span></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="1" height="20"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td width="1" height="30"></td>
                    </tr>
                </table>


                <table width="100%" align="center" border="0"
                       style="border-collapse:collapse;max-width: 680px;min-width: 300px;">
                    <tr>
                        <td>
                            <span style="line-height:25px; font-weight:300;color: #676767;font-size: 17px;"
                                  class="center">До встречи в будущих гивах!</span>
                        </td>
                    </tr>
                    <tr>
                        <td width="1" height="30"></td>
                    </tr>
                </table>


                <table width="100%" align="center" border="0"
                       style="border-collapse:collapse;max-width: 250px;min-width: 300px;">
                    <tr>
                        <td width="1" height="30"></td>
                    </tr>


                </table>

                <table width="100%" align="center" style="border-collapse:collapse;max-width: 740px;" border="0"
                       bgcolor="#232323">
                    <tr>
                        <td width="1" height="40"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <a href="#" target="_blank" style="text-decoration: none;">
                                <img src="https://givecorp.ru/img/logo_mail.png" alt=""/>
                            </a>
                        </td>
                    </tr>


                    <tr>
                        <td align="center">
                            <span style="color: #676767;">Все права защищены</span>
                        </td>
                    </tr>

                    <tr>
                        <td width="1" height="20"></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>