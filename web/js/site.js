document.body.className = navigator.platform
var popupBtns = document.querySelectorAll('.js-popup');

for (var i = 0; i < popupBtns.length; i++) {
    popupBtns[i].addEventListener('click', function (evt) {
        var popup = evt.target.dataset.popup;

        document.getElementById(popup).style.display = 'flex'
    })
}

$(document).on('click', '#checkbox-checked', function () {
    console.log($('#agree').is(':checked'));
    if ($('#agree').is(':checked') === true) {
        $('.popup__submit').attr('disabled', 'disabled');
        $('.popup__submit').css('opacity', '0.5')
    } else {
        $('.popup__submit').css('opacity', '1')
        $('.popup__submit').attr('disabled', false)
    }
});
var popupBtns = document.querySelectorAll('.js-close-popup');

$('#popup-thank button').on('click', function () {
    window.location.href = '/';
});
for (var i = 0; i < popupBtns.length; i++) {
    popupBtns[i].addEventListener('click', function (evt) {
        var popup = evt.target.dataset.popup;

        document.getElementById(popup).style.display = 'none'
    })
}

/*var textElement = document.querySelector('.number__people')
var count = +textElement.textContent;
textElement.textContent = sep(count)

setInterval(function () {

    var randAdd = Math.floor(Math.random() * 10);
    count += randAdd;

    textElement.textContent = sep(count)
}, 700)

function sep(str) {
    return str.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,')
}*/
$('form').submit(function() {
    $(this).find("button[type='submit']").prop('disabled',true);
});

(function ($) {
    $('.js-cookie-agree').on('click', function () {
        var date = new Date(Date.now() + 365 * 24 * 60 * 60 * 1000);
        date = date.toUTCString();
        document.cookie = "cookie_agree=yes; expires=" + date;
        console.log(document.cookie);
        $(this).parent().hide();
        return false;
    });
})(jQuery)
