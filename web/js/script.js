// A reference to Stripe.js
var stripe;

var orderData = {
    items: [{id: "photo-subscription"}],
    currency: "usd",
};

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
// Disable the button until we have Stripe set up on the page
document.querySelector("button").disabled = true;
$.ajax({
    url: '/stripe/create-payment-intent',
    type: 'post',
    body: JSON.stringify(orderData),
    data: {user_id: getUrlParameter('user_id')},
    success: function (response) {
        if (response.error === true) {
            document.querySelector("button").disabled = true;
            document.querySelector("button").textContent = 'User not found pls try again';
        } else {
            var elements = setupElements();
            document.querySelector("button").disabled = false;

            // Handle form submission.
            var form = document.getElementById("payment-form");
            form.addEventListener("submit", function (event) {
                event.preventDefault();
                // Initiate payment when the submit button is clicked
                pay(elements.stripe, elements.card, response.clientSecret);
            });
        }
    }
});

// Set up Stripe.js and Elements to use in checkout form
var setupElements = function () {
    stripe = Stripe('pk_live_lCPEn9JA3fQ9VUhn53pwmUCP00UjEwbQyX');
    var elements = stripe.elements();
    var style = {
        base: {
            color: "#32325d",
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#aab7c4"
            }
        },
        invalid: {
            color: "#fa755a",
            iconColor: "#fa755a"
        }
    };

    var card = elements.create("card", {style: style});
    card.mount("#card-element");

    return {
        stripe: stripe,
        card: card,
        clientSecret: '${photo-subscription}_secret_${sk_live_uElYk80sy9xSZSadZXCsKI3z00TNZqDgNL}'
    };
};

/*
 * Calls stripe.confirmCardPayment which creates a pop-up modal to
 * prompt the user to enter extra authentication details without leaving your page
 */
var pay = function (stripe, card, clientSecret) {
    changeLoadingState(true);

    // Initiate the payment.
    // If authentication is required, confirmCardPayment will automatically display a modal
    stripe
        .confirmCardPayment(clientSecret, {
            payment_method: {
                card: card
            }
        })
        .then(function (result) {
            if (result.error) {
                // Show error to your customer
                showError(result.error.message);
            } else {
                // The payment has been processed!
                orderComplete(clientSecret);
            }
        });
};

/* ------- Post-payment helpers ------- */

/* Shows a success / error message when the payment is complete */
var orderComplete = function (clientSecret) {
    // Just for the purpose of the sample, show the PaymentIntent response object
    stripe.retrievePaymentIntent(clientSecret).then(function (result) {
        var paymentIntent = result.paymentIntent;
        var paymentIntentJson = JSON.stringify(paymentIntent, null, 2);

        document.querySelector(".sr-payment-form").classList.add("hidden");
        document.querySelector(".after_div").textContent = 'thank you for participating in our giveaway!';
        fbq('track', 'Purchase', {
            value: 990,
            currency: 'USD',
        });
        document.querySelector(".sr-result").classList.remove("hidden");
        setTimeout(function () {
            document.querySelector(".sr-result").classList.add("expand");
            $(document).find('.sr-result').append("<a href='/'><button id=\"button-text\">Back</button></a>");

        }, 200);

        changeLoadingState(false);
    });
};

var showError = function (errorMsgText) {
    changeLoadingState(false);
    var errorMsg = document.querySelector(".sr-field-error");
    errorMsg.textContent = errorMsgText;
    setTimeout(function () {
        errorMsg.textContent = "";
    }, 4000);
};

// Show a spinner on payment submission
var changeLoadingState = function (isLoading) {
    if (isLoading) {
        document.querySelector("button").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#button-text").classList.add("hidden");
    } else {
        document.querySelector("button").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
    }
};
