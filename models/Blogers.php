<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blogers".
 *
 * @property int $id
 * @property string $instagram
 * @property string $name
 * @property string $count_subscribes
 * @property string $text
 * @property string $avatar
 * @property string $statistics_img
 * @property string $statistics_img_two
 * @property int $is_star
 * @property int $is_star_promotion
 * @property string $instagram_name
 *
 * @property GiveCompleted[] $giveCompleteds
 * @property Gives[] $gives
 */
class Blogers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blogers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'avatar', 'instagram_name','statistics_img','statistics_img_two'], 'string'],
            [['is_star', 'is_star_promotion'], 'boolean'],
            [['instagram', 'name', 'count_subscribes'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instagram' => 'Инстаграм ссылка',
            'name' => 'Имя Фимилия',
            'count_subscribes' => 'К-тво подписчиков',
            'text' => 'Описание',
            'is_star' => 'Топовый Блогер',
            'is_star_promotion' => 'Топовый Блогер(Реклама)',
            'instagram_name' => 'Инстаграм ник',
            'statistics_img' => 'Картинка статистики',
            'statistics_img_two' => 'Картинка статистики GEO',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiveCompleteds()
    {
        return $this->hasMany(GiveCompleted::className(), ['bloger_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGives()
    {
        return $this->hasMany(Gives::className(), ['bloger_id' => 'id']);
    }

    public function getPromotion()
    {
        return $this->hasOne(Promotion::className(), ['bloger_id' => 'id']);
    }
}
