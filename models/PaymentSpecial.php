<?php

namespace app\models;

use app\components\MyUrlManager;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "payment_ler".
 *
 * @property int $id
 * @property string $date_create
 * @property int $user_id
 * @property int $cost
 * @property User $user
 */
class PaymentSpecial extends Model
{
    public $name;
    public $inn;
    public $kpp;
    public $rs;
    public $bank;
    public $ks;
    public $bik;
    public $FIO;
    public $phone;
    public $email;

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
           // [['name', 'inn', 'kpp', 'rs', 'bank', 'ks', 'bik', 'FIO', 'phone', 'email'], 'required'],
            [['name', 'inn', 'kpp', 'rs', 'bank', 'ks', 'bik', 'FIO', 'phone', 'email'], 'safe'],
        ];
    }

}
