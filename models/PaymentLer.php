<?php

namespace app\models;

use app\components\MyUrlManager;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "payment_ler".
 *
 * @property int $id
 * @property string $date_create
 * @property int $user_id
 * @property int $cost
 * @property int $stripe_id
 * @property int $status
 * @property User $user
 */
class PaymentLer extends ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'cost', 'stripe_id', 'status', 'date_create'], 'safe'],
        ];
    }

    public function getLabelCssClassByStatus()
    {
        return $this->status == self::STATUS_ACTIVE ? 'label label-primary' : 'label label-danger';
    }

    public function getStatusString()
    {

        $statuses = self::getStatusesArray();

        if (isset($statuses[$this->status])) {

            return $statuses[$this->status];
        }

        return 'Ожидание';
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_ACTIVE => 'Оплачен',
            self::STATUS_INACTIVE => 'Не оплачен',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Date Payment',
            'user_id' => 'Member ID',
            'cost' => 'Cost',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'member_id']);
    }

}
