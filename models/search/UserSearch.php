<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\helpers\VarDumper;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role'], 'integer'],
            [['first_name', 'last_name', 'patronymic', 'email', 'instagram_ak', 'target', 'password', 'payment_status'], 'safe'],
        ];
    }

    public $payment_status;

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->leftJoin('payment', 'users.id = user_id');
        // add conditions that should always apply here
        $get = \Yii::$app->request->get();
        if (isset($get['status'])) {
            $query->where(['users.status' => $get['status']]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (isset($params['payment_status'])) {
            $query->andFilterWhere(['payment.status' => $get['payment_status']]);

        }
        $dataProvider->setSort([
            'attributes' => [
                'payment_status' => [
                    'asc' => ['payment.status' => SORT_ASC],
                    'desc' => ['payment.status' => SORT_DESC],
                    //  'label' => 'payment_status',
                    'default' => SORT_ASC
                ],
                'country_id'
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'users.id' => $this->id,
            'role' => $this->role,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'patronymic', $this->patronymic])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'instagram_ak', $this->instagram_ak])
            ->andFilterWhere(['like', 'target', $this->target])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
}
