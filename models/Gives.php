<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "gives".
 *
 * @property int $id
 * @property int $bloger_id
 * @property int $bloger_id_sec
 * @property int $count_sub
 * @property string $date_start
 * @property string $date_end
 * @property int $count_seats
 * @property int $cost
 * @property int $title
 * @property int $is_main
 * @property boolean $is_hot
 * @property boolean $is_top
 * @property boolean $is_on
 * @property int $status
 * @property int $plus_subscriber
 * @property string $presents
 * @property string $video_link
 * @property string $image
 * @property string $subscribes_in
 * @property string $sort
 * @property float $all_cost
 * @property GiveHasUser[] $giveHasUsers
 * @property GiveHasUser[] $getGiveHasUsersCustom
 * @property Blogers $bloger
 */
class Gives extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_END = 2;

    /**
     * {@inheritdoc}
     */
    public function statuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_END => 'Закончился',
        ];
    }

    public static function tableName()
    {
        return 'gives';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bloger_id', 'bloger_id_sec', 'count_sub', 'count_seats', 'status', 'plus_subscriber', 'sort'], 'integer'],
            [['cost', 'all_cost', 'image', 'is_hot','is_top','is_on'], 'safe'],
            [['date_start', 'date_end', 'title', 'video_link', 'presents', 'subscribes_in'], 'safe'],
            [['is_main'], 'boolean'],
            [['bloger_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogers::className(), 'targetAttribute' => ['bloger_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bloger_id' => 'Блогер',
            'bloger_id_sec' => 'Блогер_2',
            'count_sub' => 'Подписчиков пришло',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окночания',
            'count_seats' => 'К-тво мест',
            'cost' => 'Стоимость подписчика',
            'title' => 'Название',
            'is_star' => 'В топе',
            'video_link' => 'Ссылка на видео',
            'presents' => 'Подарки',
            'subscribes_in' => 'Приход подписчиков',
            'status' => 'Статус',
            'is_main' => 'На главную',
            'plus_subscriber' => 'Накрутка(Это число + реальные подписчики)',
            'all_cost' => 'Полная цена',
            'image' => 'Превью видео',
            'sort' => 'Сортировка',
            'is_hot' => 'Горячий гив',
            'is_top' => 'Топовый гив',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiveHasUsers()
    {
        return $this->hasMany(GiveHasUser::className(), ['give_id' => 'id']);
    }

    public function getGiveHasUsersCustom()
    {
        return $this->hasMany(GiveHasUser::className(), ['give_id' => 'id'])->andWhere(['user_id' => Yii::$app->user->identity->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloger()
    {
        return $this->hasOne(Blogers::className(), ['id' => 'bloger_id']);
    }

    public function getBlogerSec()
    {
        return $this->hasOne(Blogers::className(), ['id' => 'bloger_id_sec']);
    }
}
