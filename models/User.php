<?php

namespace app\models;

use app\components\MyUrlManager;
use DeepCopy\f006\A;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $email_status
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property integer $role
 * @property integer $telegramm_see
 * @property string $promo
 * @property string $instagram_ak
 * @property string $target
 * @property string $status
 * @property string $phone
 * @property string $invoiceId
 * @property PaymentLer $payment
 * @property GiveHasUser[] $giveHasUsersCustom
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;
    const SCENARIO_CHANGE_PASSWORD_USER = 'change_password_user';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $orig_password;
    public $new_password;
    public $password1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHANGE_PASSWORD_USER] = ['orig_password', 'new_password', 'password1'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name', 'patronymic', 'phone', 'instagram_ak', 'target'], 'safe'],
            [['target', 'instagram_ak', 'status', 'password', 'invoiceId', 'email_status', 'refferer_link', 'telegramm_see', 'login_date'], 'safe'],
            [['email'], 'email'],
            [['email'], 'unique', 'message' => '{attribute} уже занят!'],
            ['password1', 'compare',
                'compareAttribute' => 'new_password',
                'on' => self::SCENARIO_CHANGE_PASSWORD_USER,
                'message' => 'Пароли не совпадают'
            ],
            ['orig_password', 'myCompare', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER],
            [['orig_password', 'new_password', 'password1'], 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Пароль',
            'first_name' => ' Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'status' => 'Статус',
            'target' => 'Цель участия',
            'orig_password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
            'password1' => 'Новый пароль',
            'phone' => 'Телефон',
            'login_status' => 'Статус авторизации',
            'login_date' => 'Дата авторизации',
            'refferer_link' => 'Переход по ссылке',
            'email_status' => 'Получил ли письмо клиент',
            'telegramm_see' => 'Видил ли попап',
        ];
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return User::findOne(['id' => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->authKey;

    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function myCompare($attribute, $params)
    {
        if ($this->$attribute != $this->password) {
            $this->addError($attribute, 'Старый пароль не совпадает');
        }
    }

    public function updatePassword($new_password)
    {
        $this->password = $new_password;
    }

    public function getUserName()
    {
        if ($this->first_name && $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        }
        return $this->email;
    }

    public function getUserFIO()
    {
        return $this->first_name . ' ' . $this->last_name . ' ' . $this->patronymic;
    }

    public static function findByEmail($email)
    {
        return User::findOne(['email' => $email]);
    }

    public function login()
    {
        return Yii::$app->user->login($this);
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function getStatusString()
    {

        $statuses = self::getStatusesArray();

        if (isset($statuses[$this->status])) {

            return $statuses[$this->status];
        }

        return 'Ожидание';
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_ACTIVE => 'Консультация',
            self::STATUS_INACTIVE => 'Не оплачен',
        ];
    }

    public function getLabelCssClassByStatus()
    {
        return $this->status == self::STATUS_ACTIVE ? 'label label-primary' : 'label label-danger';
    }

    public function getPayment()
    {
        return $this->hasOne(PaymentLer::class, ['user_id' => 'id']);
    }

    public function getGiveHasUsersCustom($give_id)
    {
        return $this->hasMany(GiveHasUser::className(), ['user_id' => 'id'])->andWhere(['give_id' => $give_id])->all();
    }

    static function russian_date($date)
    {
        $date = explode(".", $date);
        switch ($date[1]) {
            case 1:
                $m = 'января';
                break;
            case 2:
                $m = 'февраля';
                break;
            case 3:
                $m = 'марта';
                break;
            case 4:
                $m = 'апреля';
                break;
            case 5:
                $m = 'мая';
                break;
            case 6:
                $m = 'июня';
                break;
            case 7:
                $m = 'июля';
                break;
            case 8:
                $m = 'августа';
                break;
            case 9:
                $m = 'сентября';
                break;
            case 10:
                $m = 'октября';
                break;
            case 11:
                $m = 'ноября';
                break;
            case 12:
                $m = 'декабря';
                break;
        }
        return $date[0] . '&nbsp;' . $m;
    }
}
