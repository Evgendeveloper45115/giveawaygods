<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "promotion".
 *
 * @property int $id
 * @property int $bloger_id
 * @property int $auditory
 * @property string $interest
 * @property string $count_seat
 * @property int $status
 * @property int $plus_subscriber
 * @property string $sort
 * @property float $all_cost
 * @property PromotionHasUser[] $promotionHasUsers
 * @property Blogers $bloger
 */
class Promotion extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_END = 2;

    /**
     * {@inheritdoc}
     */
    public function statuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_END => 'Закончился',
        ];
    }

    public static function tableName()
    {
        return 'promotion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bloger_id', 'count_seat', 'status', 'plus_subscriber', 'sort'], 'integer'],
            [['all_cost', 'auditory', 'interest'], 'safe'],
            [['bloger_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogers::className(), 'targetAttribute' => ['bloger_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bloger_id' => 'Блогер',
            'count_seat' => 'К-тво мест',
            'status' => 'Статус',
            'plus_subscriber' => 'Накрутка(Это число + реальные подписчики)',
            'all_cost' => 'Полная цена',
            'sort' => 'Сортировка',
            'auditory' => 'Аудитория',
            'interest' => 'Интересы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotionHasUsers()
    {
        return $this->hasMany(PromotionHasUser::className(), ['promotion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloger()
    {
        return $this->hasOne(Blogers::className(), ['id' => 'bloger_id']);
    }
}
